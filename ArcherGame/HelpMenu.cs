﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

using RC_Framework;
using Sprite3Extension;
using System.Threading;
using Microsoft.Xna.Framework.Media;
namespace ArcherGame
{
    class HelpMenu : RC_GameStateParent
    {

        Song music;
        Texture2D image;

        public override void LoadContent()
        {
            font1 = Content.Load<SpriteFont>("SpriteFont1");
            image = Content.Load<Texture2D>("helpScreen2");
        }

        public override void EnterLevel(int fromLevelNum)
        {

        }

        public override void ExitLevel()
        {
            
        }

        public override void UnloadContent()
        {
        
        }

        public override void Update(GameTime gameTime) 
        {
            getKeyboardAndMouse();
            if (keyState.IsKeyDown(Keys.F1) && !prevKeyState.IsKeyDown(Keys.F1))
                gameStateManager.setLevel(1);

        }
        public override void Draw(GameTime gameTime)
        {
            graphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();
            float scale = Math.Min((float)Window.ClientBounds.Width / image.Width, (float)Window.ClientBounds.Height / image.Height);
            int width = (int)(image.Width * scale);
            int height = (int)(image.Height * scale);
            spriteBatch.Draw(image, new Rectangle(Window.ClientBounds.Width / 2 - width / 2, Window.ClientBounds.Height / 2 - height / 2, width, height), Color.White);
            spriteBatch.End();

        }
    }
}
