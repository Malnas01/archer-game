﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using RC_Framework;
using Sprite3Extension;
namespace ArcherGame
{
    class Collectable : Sprite3
    {
        public Action<Archer> OnCollect;

        public Collectable(float x, float y)
            : base()
        {
            pos = new Vector2(x, y);
            visible = true;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            animationTick();
            tick();
        }

        public virtual void Collect(Archer player)
        {
            if (visible)
            {
                if (OnCollect != null)
                    OnCollect(player);
                visible = false;
                this.active = false;
            }
        }
    }
}
