﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

using RC_Framework;
using Sprite3Extension;
using System.Threading;
#endregion

namespace ArcherGame
{
    //Anzel was here 2015 yea
    //Simon was here
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        RC_GameStateManager stateManager;
        public static GameWindow window;
        
        public static bool test = false;

        //public static Game1 instance;

        public Game1()
            : base()
        {
            window = Window;
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 600;

            Window.Title = "Mist Game";
            Window.AllowUserResizing = true;
            Window.ClientSizeChanged += new EventHandler<EventArgs>(OnClientSizeChanged);

            //instance = this;
            
        }

        public void OnClientSizeChanged(object sender, EventArgs e)
        {

            
            PresentationParameters pres = GraphicsDevice.PresentationParameters;

            pres.BackBufferWidth = Window.ClientBounds.Width;
            pres.BackBufferHeight = Window.ClientBounds.Height;

            GraphicsDevice.Reset(pres); 
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            LineBatch.init(GraphicsDevice);
            base.Initialize();
        }
        /*
        public void TurboUpdate(bool on)
        {
            if (on)
            {
                this.IsFixedTimeStep = false;
                this.TargetElapsedTime = TimeSpan.FromMilliseconds(120);
            }
            else
            {
                this.IsFixedTimeStep = true;
                this.ResetElapsedTime();
            }

        } */

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            //this.IsMouseVisible = true;

            PresentationParameters pres = GraphicsDevice.PresentationParameters;

            pres.BackBufferWidth = Window.ClientBounds.Width;
            pres.BackBufferHeight = Window.ClientBounds.Height;

            GraphicsDevice.Reset(pres);

            spriteBatch = new SpriteBatch(GraphicsDevice);
            stateManager = new RC_GameStateManager();
            stateManager.AddLevel(0, new Menu());
            stateManager.AddLevel(1, new PlayState());
            stateManager.AddLevel(2, new HelpMenu());

            for (int i = 0; i < stateManager.LevelCount(); i++)
            {
                stateManager.getLevel(i).InitializeLevel(GraphicsDevice,spriteBatch,Content, stateManager, Window);
                stateManager.getLevel(i).LoadContent();
            }
            stateManager.setLevel(0);
            // Create a new SpriteBatch, which can be used to draw textures.
            
            
            /*
            floor = new Sprite3(true, Content.Load<Texture2D>("ground"), 400, 300);
            floor.setBBToTexture();
            floor.name = "floor";

            floor2 = new Sprite3(true, Content.Load<Texture2D>("ground"), 250, 500);
            floor2.setBBToTexture();
            floor2.name = "floor2";

            wall = new Sprite3(true, Content.Load<Texture2D>("ground"), 400, 300);
            wall.displayAngle = MathHelper.PiOver2;
            wall.setBBToTexture();
            wall.name = "wall";

            slope = new Sprite3(true, Content.Load<Texture2D>("ground"), 400, 300);
            slope.displayAngle = -MathHelper.PiOver4;
            slope.setBBToTexture();
            slope.setBoundingPolyFromBB();
            //slope.boundingPoly.rotatePolygon12(slope.getPos(), slope.displayAngle);
            slope.name = "slope";
            boundingPoly = slope.boundingPoly;

            font1 = Content.Load<SpriteFont>("SpriteFont1");
            */
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            else 
            stateManager.getCurrentLevel().Update(gameTime);

            base.Update(gameTime);
        }

        int flipInt(Sprite3 sprite)
        {
            return (sprite.flip == SpriteEffects.FlipHorizontally ? -1 : 1);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            stateManager.getCurrentLevel().Draw(gameTime);
            base.Draw(gameTime);
        }
    }
}
