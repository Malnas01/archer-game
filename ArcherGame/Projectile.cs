﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

using RC_Framework;
using Sprite3Extension;

namespace ArcherGame
{
    class Projectile : Sprite3
    {
        public Projectile(bool visibleZ, Texture2D texZ, float x, float y)
            : base(visibleZ, texZ, x, y)
        {

        }

        public virtual void CollideEnemy(List<Enemy> collideables)
        {

        }

        public virtual void CollideLevel(List<Sprite3> collideables)
        {
            
            
        }

    }
}
