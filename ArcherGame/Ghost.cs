﻿using RC_Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

using Sprite3Extension;

namespace ArcherGame
{
    class Ghost : Enemy
    {

        static Texture2D texture;
        new Archer target;
        float speed = 6;
        float dmg = 10;

        bool targetFound;
        int loseTimer = 0;
        int timeTillLose = 240;
        float lineOfSight = 2500;

        int knockbackTime = 20;
        int knockbackTimer;
        Vector2 knockback;
        bool dead;

        List<Sprite3> collideables;


        public Ghost(float x, float y, ContentManager c, Archer target, float scale = 1) : base() 
        {
            knockbackTimer = 0;
            visible = true;
            if(texture == null || texture.IsDisposed)
                setTexture(texture = c.Load<Texture2D>("ghost"), true);
            else
                setTexture(texture, true);
            pos = oldPos = new Vector2(x, y);
            //setHSoffset(new Vector2(192, 192));

            setWidth(getWidth() * scale);
            setHeight(getHeight() * scale);

            setBBandHSFractionOfTexCentered(0.7f);
            this.target = target;

            //this.setBB(135, 70, 100, 200);

            deltaSpeed.X = speed;

            health = 20;
        }


        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            base.Update(gameTime);


            if (!dead)
            {
                animationTick();


                if (knockbackTimer <= 0)
                {
                    if (Vector2.Distance(target.getPos(), pos) <= lineOfSight)
                    {
                        targetFound = true;
                        loseTimer = 0;
                    }
                    else
                    {

                        if (loseTimer >= timeTillLose)
                        {
                            targetFound = false;
                        }
                        else
                            loseTimer++;
                    }

                    if (targetFound)
                    {
                        float temp;
                        deltaSpeed.X = Math.Abs(deltaSpeed.X) * ((target.getPosX() - pos.X) < 0 ? -1 : 1);
                        deltaSpeed.Y = Math.Abs(deltaSpeed.Y) * ((target.getPosY() - pos.Y) < 0 ? -1 : 1);
                        moveByDeltaXY();
                        if ((ticks & 1) == 1)
                        {
                            temp = deltaSpeed.X;
                            deltaSpeed.X = deltaSpeed.Y;
                            deltaSpeed.Y = temp;
                        }
                    }
                    else
                    {
                    }
                }
                else
                {
                    knockbackTimer--;
                    deltaSpeed = knockback;
                    moveByDeltaXY();
                    float absX = Math.Abs(knockback.X);
                    float absY = Math.Abs(knockback.Y);
                    knockback.X = (absX - absX * 1 / ((knockbackTimer + 1))) * Math.Sign(knockback.X);
                    knockback.Y = (absY - absY * 1 / ((knockbackTimer + 1))) * Math.Sign(knockback.Y);

                    if (knockbackTimer <= 0)
                        deltaSpeed = new Vector2(speed, 0);
                }
            }
            else
            {
                setColor(Color.LightBlue * (TicksToInvisible/200f));
            }
        }

        bool HasLOS()
        {

            Line sight = new Line(pos, target.getPos());
            if(collideables != null)
                foreach (Sprite3 c in collideables)
                {
                    if (c.Collides(sight))
                        return false;
                }
            return true;
        }

        void KnockBack(float angle, float strength)
        {
            knockback = new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));
            knockback *= strength;
            knockbackTimer = knockbackTime;
        }

        void KnockBack(float angle, float strength, int time)
        {
            knockback = new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));
            knockback *= strength;
            knockbackTimer = time;
        }

        void KnockBack(Vector2 dir, float strength)
        {
            knockback = dir;
            knockback.Normalize();
            knockback *= strength;
            knockbackTimer = knockbackTime;
        }

        void KnockBack(Vector2 dir, float strength , int time)
        {
            knockback = dir;
            knockback.Normalize();
            knockback *= strength;
            knockbackTimer = time;
        }

        public override bool Hurt(float amount, Sprite3 source = null)
        {
            if (!dead)
            {
                if (source.GetType() == typeof(Arrow))
                {
                    KnockBack(source.displayAngle, ((Arrow)source).power * 0.8f);
                    targetFound = true;
                    loseTimer = 0;
                }
                else
                {
                    KnockBack(pos - target.getPos(), 15);
                }

                return base.Hurt(amount, source);
            }
            else
                return false;
        }

        public override void Die()
        {
            setTicksToInvisible(200, true);
            dead = true;
            
        }

        
        public override bool Collides(Rectangle r)
        {
            if (r.Intersects(getBoundingBoxAA())) return true;
            return false;
        }

        public override bool Collides(Polygon12 p)
        {
            if (p.Intersects(getBoundingBoxAA())) return true;
            return false;
        }

        public override bool Collides(Line l)
        {
            if (l.Intersects(getBoundingBoxAA())) return true;
            return false;
        }

        public override void CollidePlayer(Archer player)
        {
            if (player.Collides(getBoundingBoxAA()) && knockbackTimer <= 0)
            {
                player.Hurt(dmg, new Vector2(Math.Sign(target.getPos().X - pos.X) * 30, -60));
                KnockBack(pos - target.getPos(), 10);
            }
        }

        public override void CollideLevel(List<Sprite3> collideables)
        {

            this.collideables = collideables;
            /*
            foreach (Sprite3 c in collideables)
            {
                if (c.boundingPoly == null && c.getDeltaSpeed() == Vector2.Zero && c.getMoveSpeed() == 0)
                {
                    if (c.Collides(getBoundingBoxAA()))
                    {
                        pos = oldPos;
                    }
                }
                else
                {
                    Rectangle BB = getBoundingBoxAA();
                    while(c.Collides(new Vector2(BB.Center.X,BB.Top))) {
                        pos.Y += speed;
                        BB.Y += (int)speed;
                    }
                    while (c.Collides(new Vector2(BB.Right, BB.Center.Y)))
                    {
                        pos.X -= speed;
                        BB.X -= (int)speed;
                    }
                    while (c.Collides(new Vector2(BB.Center.X, BB.Bottom)))
                    {
                        pos.Y -= speed;
                        BB.Y -= (int)speed;
                    }
                    while (c.Collides(new Vector2(BB.Left, BB.Center.Y)))
                    {
                        pos.X += speed;
                        BB.X += (int)speed;
                    }
                }

            }
            */
        }

        public override void Draw(SpriteBatch sb)
        {
            base.Draw(sb);
        }

    }
}
