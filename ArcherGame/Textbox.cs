﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArcherGame
{
    class Textbox
    {

        static SpriteFont defaultFont;
        static Texture2D defaultTex;

        public Texture2D tex;
        public SpriteFont font;

        public Rectangle rect;
        
        private KeyboardState oldKeyboardState;
        private KeyboardState currentKeyboardState;

        public Color boxColor;
        public Color textColor;
        public Color cursorColor;
        public Color greyTextColor;

        public int limit = 0;
        int cursorFlashSpeed = 20;
        int cursorTicker = 0;
        int cursorPos = 0;
        bool cursor = true;

        public Action OnGainFocus;
        public Action OnLoseFocus;

        public string emptyText;

        public int padding = 30;

        protected bool _focus;
        public bool Focus {
            get { return _focus;}
            set {
                _focus = value;
                currentKeyboardState = Keyboard.GetState();
                if (_focus)
                {
                    if (OnGainFocus != null)
                        OnGainFocus();
                }
                else if(OnLoseFocus != null)
                    OnLoseFocus();
            }
        }
        public string Text { get; protected set; }

        public Textbox(Rectangle rect, ContentManager Content, string emptyText = "", int limit = 0)
        {
            Text = String.Empty;
            this.rect = rect;
            if (defaultTex == null || defaultTex.IsDisposed)
                defaultTex = Content.Load<Texture2D>("healthbar");
            this.tex = defaultTex;
            if (defaultFont == null || defaultFont.Texture.IsDisposed)
                defaultFont = Content.Load<SpriteFont>("SpriteFont1");
            this.font = defaultFont;
            this.limit = limit;
            this.emptyText = emptyText;

            boxColor = Color.White;
            textColor = Color.White;
            cursorColor = Color.White;
            greyTextColor = Color.LightGray;
        }

        public Textbox(Rectangle rect, SpriteFont font, ContentManager Content, string emptyText = "", int limit = 0) : this(rect,Content,emptyText,limit)
        {
            this.font = font;
        }

        public Textbox(Rectangle rect, Texture2D tex, ContentManager Content, string emptyText = "", int limit = 0) : this(rect,Content,emptyText,limit)
        {
            this.tex = tex;
        }

        public Textbox(Rectangle rect, Texture2D tex, SpriteFont font, string emptyText = "", int limit = 0)
        {
            Text = String.Empty;
            this.rect = rect;
            this.tex = tex;
            this.font = font;
            this.limit = limit;
            this.emptyText = emptyText;

            boxColor = Color.White;
            textColor = Color.White;
            cursorColor = Color.White;
            greyTextColor = Color.LightGray;
        }

        public void Update()
        {
            if (Focus)
            {
                oldKeyboardState = currentKeyboardState;
                currentKeyboardState = Keyboard.GetState();

                Keys[] pressedKeys = currentKeyboardState.GetPressedKeys();

                foreach (Keys key in pressedKeys)
                {
                    if (oldKeyboardState.IsKeyUp(key))
                    {
                        if (key == Keys.Back && cursorPos > 0)
                        {
                            Text = Text.Remove(cursorPos - 1, 1);
                            cursorPos--;
                        }
                        else if (key == Keys.Delete && cursorPos < Text.Length)
                            Text = Text.Remove(cursorPos, 1);
                        else if (key == Keys.Space && (limit == 0 || Text.Length < limit))
                            Text = Text.Insert(cursorPos++, " ");
                        else if (key == Keys.Enter)
                            Focus = false;
                        else if (key == Keys.Left)
                            cursorPos = Math.Max(0, cursorPos - 1);
                        else if (key == Keys.Right)
                            cursorPos = Math.Min(cursorPos + 1, Text.Length);
                        else if (key == Keys.Home)
                            cursorPos = 0;
                        else if (key == Keys.End)
                            cursorPos = Text.Length;
                        else if (limit == 0 || Text.Length < limit)
                        {
                            string keyString = key.ToString();
                            if (keyString.Length == 1)
                            {
                                string insert = keyString.ToUpper();
                                if (cursorPos > 0 && Text[cursorPos - 1] != ' ')
                                    insert = keyString.ToLower();
                                else if (cursorPos < Text.Length - 1)
                                {
                                    insert += char.ToLower(Text[cursorPos]);
                                    Text = Text.Remove(cursorPos, 1);
                                }
                                Text = Text.Insert(cursorPos, insert);
                                cursorPos++;
                            }
                        }

                    }
                }
            }
            
            //if (currentKeyboardState.IsKeyDown(Keys.F4) && !oldKeyboardState.IsKeyDown(Keys.F4))
            //   Game1.instance.TurboUpdate(Game1.instance.IsFixedTimeStep);
        }

        public void Draw(SpriteBatch sb)
        {
            string visibleString;
            Color c;
            if ((Text == "" || Text == null)&& !Focus && emptyText != null)
            {
                visibleString = emptyText;
                c = Color.LightGray;
            }
            else
            {
                visibleString = Text;
                c = Color.White;
            }

            float scale = (float)rect.Height/font.LineSpacing;
            int cursorOffset = Text.Length - cursorPos;
            if(limit != 0)
                while (font.MeasureString(visibleString).X > (rect.Width - padding *2) / scale)
                {
                    if (visibleString.Length > cursorOffset)
                        visibleString = visibleString.Remove(0, 1);
                    else
                    {
                        visibleString = visibleString.Remove(visibleString.Length - 1, 1);
                        cursorOffset--;
                    }
                }


            sb.Draw(tex, rect, Color.White);
            if(visibleString.Length > 0)
                sb.DrawString(font, visibleString, new Vector2(rect.X + padding, rect.Y), c, 0, new Vector2(0, 0), scale, SpriteEffects.None, 0);

            if (Focus)
            {
                if (cursor)
                {

                    Vector2 dist = font.MeasureString(visibleString.Remove(visibleString.Length - cursorOffset, cursorOffset));
                    sb.DrawString(font, "|", new Vector2(rect.X + dist.X * scale + padding/2, rect.Y), Color.White, 0, new Vector2(0, 0), scale, SpriteEffects.None, 0);
                }

                if (cursorTicker >= cursorFlashSpeed)
                {
                    cursor = !cursor;
                    cursorTicker = 0;
                }

                cursorTicker++;
            }
        }

        /// <summary>
        /// Call this to load the default texture and font for the Textbox class.
        /// Called automatically by first instance of textbox
        /// </summary>
        /// <param name="Content"></param>
        public static void LoadDefaults(ContentManager Content) {

            if (defaultTex == null)
                defaultTex = Content.Load<Texture2D>("healthbar");
            if (defaultFont == null)
                defaultFont = Content.Load<SpriteFont>("SpriteFont1");
        }


        public static void DrawTextBox(SpriteBatch sb, Rectangle rect, string text, Color c, int padding = 30) {
            float scale = (float)rect.Height / defaultFont.LineSpacing;

            while (defaultFont.MeasureString(text).X > (rect.Width - padding *2) / scale)
            {
                text = text.Remove(text.Length - 1, 1);
            }

            sb.Draw(defaultTex, rect, Color.Gray);
            if (text.Length > 0)
                sb.DrawString(defaultFont, text, new Vector2(rect.X + padding, rect.Y), c, 0, new Vector2(0, 0), scale, SpriteEffects.None, 0);
        }
    }
}
