﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

using RC_Framework;
using Sprite3Extension;
using System.Threading;

namespace ArcherGame
{
    class Level2 : Level
    {
        Portal portal;
        public override void LoadLevel()
        {
            Sprite3 s;
            SetLoadingText("Loading Textures");
            #region Load Textures
            Texture2D stairs = Content.Load<Texture2D>("stairs");
            Texture2D floatingPlatform = Content.Load<Texture2D>("floatingPlatform");
            Texture2D longPlatform = Content.Load<Texture2D>("platform2");
            Texture2D pillar = Content.Load<Texture2D>("pillar");
            Texture2D grass = Content.Load<Texture2D>("grass");
            Texture2D wall = Content.Load<Texture2D>("wall");
            Texture2D texs = Content.Load<Texture2D>("floatingPlatform");
            Texture2D fire = Content.Load<Texture2D>("flames");
            #endregion

            SetLoadingText("Loading Backdrop");
            #region Backdrop
            skybox.Add(new SkyboxImage(Content.Load<Texture2D>("Sky"), 0, 0, 0, 0));
            skybox.Add(new SkyboxImage(Content.Load<Texture2D>("mountains"), -15, 300, 40, 0));
            background.Add(new StaticImage(Content.Load<Texture2D>("mountains"), -2500, 0, 25000, 10000, Color.LightGray, SpriteEffects.FlipHorizontally));
            #endregion

            SetLoadingText("Generating Platforms");
            #region Platforms


            collideables.Add(s = LevelHelper.Stairs(stairs, -1800, 3000, 3.3f));
            s.flip = SpriteEffects.FlipHorizontally;

            collectables.Add(new Fire(fire, -280, 3100, 2500, 800));

            collideables.Add(s=LevelHelper.Stairs(stairs, -400, 0, 3));
            s.displayAngle = 3 * MathHelper.PiOver4;

            collideables.Add(s = LevelHelper.FloatingPlatform(floatingPlatform, 1200, -400));
            s.displayAngle = MathHelper.PiOver2;
            s.flip = SpriteEffects.FlipVertically;

            collideables.Add(s = LevelHelper.FloatingPlatform(floatingPlatform, 2000, -600));
            s.displayAngle = MathHelper.PiOver2 + 1;
            s.flip = SpriteEffects.FlipVertically;

            collideables.Add(s = LevelHelper.FloatingPlatform(floatingPlatform, 2400, -300));
            s.displayAngle = MathHelper.PiOver2 - 1.2f;
            s.flip = SpriteEffects.FlipHorizontally;

            collideables.Add(s = LevelHelper.FloatingPlatform(floatingPlatform, 3600, 1800));
            s.flip = SpriteEffects.FlipHorizontally;

            collideables.Add(s = LevelHelper.FloatingPlatform(floatingPlatform, 4200, -1000));
            s.flip = SpriteEffects.FlipHorizontally;

            collideables.Add(s = LevelHelper.FloatingPlatform(floatingPlatform, 6400, -700));
            s.flip = SpriteEffects.FlipVertically;
            s.displayAngle = MathHelper.PiOver4-0.3f;

            collideables.Add(s = LevelHelper.Stairs(stairs, 1900, 3200, 3));


            collideables.Add(s =LevelHelper.LongPlatorm(longPlatform, 5200, 1100, 3));
            s.flip = SpriteEffects.FlipHorizontally;

            collideables.Add(s = LevelHelper.Stairs(stairs,11100,3000, 3));
            s.flip = SpriteEffects.FlipHorizontally;

            collectables.Add(new Fire(fire, s.getPosX() + s.getWidth() - 450, s.getPosY() +100, 2000, s.getHeight()*2 - 300));

            collideables.Add(s = LevelHelper.Stairs(stairs, stairs.Width*3 + 12300, 3000, 3));


            collideables.Add(s = LevelHelper.Pillar(pillar, 16300, 3666,12));
            s.displayAngle = MathHelper.PiOver4;

            //collideables.Add(s = LevelHelper.Pillar(pillar, 18000, 4666, 12));
            #endregion

            SetLoadingText("Spawning Monsters");
            #region Spawn Monsters
            enemies.Add(new Bat(2930, -1682, Content, player));
            enemies.Add(new Ghost(5000, 375, Content, player, 3));
            enemies.Add(new Bat(2500, -1000, Content, player));
            enemies.Add(new Ghost(7650, 2250, Content, player, 3));
            enemies.Add(new Bat(3565, -762, Content, player));
            enemies.Add(new Bat(423, 182, Content, player));
            enemies.Add(new Bat(13233, 1250, Content, player));
            enemies.Add(new Bat(14050, 1250, Content, player));
            #endregion

            SetLoadingText("Spawning Loot");
            #region Pickups
            collectables.Add(new ArrowHead(200, 1950, Content));
            collectables.Add(new ArrowHead(4200, 1547, Content));
            collectables.Add(new ArrowHead(4900, -1250, Content));
            collectables.Add(new ArrowHead(11250, 800, Content));
            collectables.Add(new ArrowHead(13600, 1733, Content));
            collectables.Add(new ArrowHead(18029, 500, Content));
            collectables.Add(portal = new Portal(19700, -940, LevelManager, Content));
            portal.visible = false;
            //collectables.Add(new Fire(fire, 1500, 3000, 2000, 800, 7, 7));
            #endregion

            #region Level Bounds
            Sprite3 ground = LevelHelper.Ground(LineBatch._empty_texture, -8192, 3000, 36384, 5000, Color.Black);
            collideables.Add(ground);

            LevelHelper.AddGrass(ground, foreground, grass, 1f, 1000, 1, 0.5f);

            collideables.Add(LevelHelper.Wall(wall, 0, 3000, 4));
            collideables.Add(LevelHelper.Wall(wall, 20000, 3000, 4, SpriteEffects.FlipHorizontally));
            #endregion
        }

        public override void SetUpLevel()
        {
            player.Reset(new Vector2(2270, 2052));
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (ArrowHead.CountIn(collectables) == 0)
                portal.visible = true;  
        }

    }
}
