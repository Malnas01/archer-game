﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RC_Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArcherGame
{
    public abstract class Camera2D
    {
        protected Vector2 _pos;
        public float Rotation { get; set; }
        public float Zoom {get;set;}
        public Matrix Transform { get; set; }
        public Matrix InverseTransform { get; protected set; }

        public GameWindow window;

        public Vector2 Pos
        {
            get { return _pos; }
            set { _pos = value; }
        }

        public Camera2D(GameWindow window)
        {
            Zoom = 1.0f;
            Rotation = 0.0f;
            Pos = Vector2.Zero;
            this.window = window;
        }

        public virtual void Update()
        {
            try{
                //Clamp zoom value
                Zoom = MathHelper.Clamp(Zoom, 0.01f, 10.0f);
                //Clamp rotation value
                Rotation = ClampAngle(Rotation);
                //Create view matrix
                Transform = Matrix.CreateTranslation(new Vector3(-_pos.X, -_pos.Y, 0)) *
                            Matrix.CreateRotationZ(Rotation) *
                            Matrix.CreateScale(new Vector3(Zoom, Zoom, 1)) *
                            Matrix.CreateTranslation(new Vector3(window.ClientBounds.Width / 2, window.ClientBounds.Height / 2, 0));
                //Update inverse matrix
                InverseTransform = Matrix.Invert(Transform);
            }catch(Exception e){}
            
        }
     
        /// <summary>
        /// Clamps a radian value between -pi and pi
        /// </summary>
        /// <param name="radians">angle to be clamped</param>
        /// <returns>clamped angle</returns>
        protected float ClampAngle(float radians)
        {
            while (radians < -MathHelper.Pi)
            {
                radians += MathHelper.TwoPi;
            }
            while (radians > MathHelper.Pi)
            {
                radians -= MathHelper.TwoPi;            
            }
            return radians;
        }

    }

    public interface IChaseable
    {
        Vector2 getPos();
    }

    public class ChaseCam : Camera2D
    {

        public Sprite3Parent target;
        public float speed;
        public Vector2 offset;

        public ChaseCam(GameWindow window, Sprite3Parent target, float speed = 0.1f)
            : this(window, target, Vector2.Zero, speed)
        {

        }

        public ChaseCam(GameWindow window, Sprite3Parent target, Vector2 offset, float speed = 0.1f)
            : base(window)
        {
            this.target = target;
            this.speed = speed;
            this.offset = offset;
        }

        public override void Update()
        {
            KeyboardState k = Keyboard.GetState();

            Vector2 targetPos = target.getPos() -offset;
           if (k.IsKeyDown(Keys.Up))
           {
               Zoom /= 0.9f;
           }
           if (k.IsKeyDown(Keys.Down))
           {
               Zoom *= 0.9f;
           }
           if (k.IsKeyDown(Keys.OemOpenBrackets))
           {
               Rotation -= 0.05f;
           }
           if (k.IsKeyDown(Keys.OemCloseBrackets))
           {
               Rotation += 0.05f;
           }
           /*if (k.IsKeyDown(Keys.Up))
           {
               _pos.Y -= speed;
           }
           if (k.IsKeyDown(Keys.Down))
           {
               _pos.Y += speed;
           }
           if (k.IsKeyDown(Keys.Left))
           {
               _pos.X -= speed;
           }
           if (k.IsKeyDown(Keys.Right))
           {
               _pos.X += speed;
           }
            
           
           Vector2 dir = targetPos - _pos;

            
           if (dir.Length() > speed)
           {
               dir.Normalize();
               _pos += dir * speed;
           }
           else
           {
               _pos = targetPos;
           }*/


            if (Vector2.DistanceSquared(_pos, targetPos) > 1)
                _pos = Vector2.Lerp(_pos, targetPos, speed);
            else
                _pos = targetPos;
            
            base.Update();
        }

    }

}
