﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using RC_Framework;
using Sprite3Extension;

namespace ArcherGame
{
    class ArrowHead : Collectable
    {

        static Texture2D texture;

        public ArrowHead(float x, float y, ContentManager Content)
            : base(x, y)
        {
            if (texture == null || texture.IsDisposed)
                setTexture(texture = Content.Load<Texture2D>("arrowHead"),true);
            else
                setTexture(texture, true);

            int frames = 9;
            setXframes(frames);
            setWidth(getWidth() / frames);
            setBB(0, 0, tex.Width / getXframes(), tex.Height / getYframes());
            setHSoffset(new Vector2(width / 2, height / 2));
            this.addAnimation("Spin", 0, frames, 5).SetPlaying();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            adjustAngles();
        }

        public static int CountIn(List<Collectable> collectables) {
            return collectables.Count(x => x.GetType() == typeof(ArrowHead));
        }
    }
}
