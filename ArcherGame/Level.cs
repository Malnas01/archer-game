﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

using RC_Framework;
using Sprite3Extension;
using System.Threading;

namespace ArcherGame
{
    abstract class Level
    {

        public LevelManager LevelManager;
        protected ContentManager Content;


        
        protected List<StaticImage> skybox;
        protected List<StaticImage> background;
        //Player is drawn in between these layers
        protected List<Collectable> collectables;
        protected List<Sprite3> collideables;
        protected List<Enemy> enemies;
        protected List<StaticImage> foreground;

        protected KeyboardState prevKeyState;
        protected KeyboardState keyState;

        protected SpriteFont font;
        protected bool loading = true;
        protected bool setUp = false;
        string loadingText = "";
        public bool debug = false;

        public static TimeSpan timer;
        public bool pauseTimer = false;

        protected Archer player;
        public Level()
        {
            collectables = new List<Collectable>();
            collideables = new List<Sprite3>();
            enemies = new List<Enemy>();

            skybox = new List<StaticImage>();
            background = new List<StaticImage>();
            foreground = new List<StaticImage>();

            if (timer == null) timer = new TimeSpan();
        }

        /// <summary>
        /// Used to load all resources
        /// </summary>
        public abstract void LoadLevel();

        public virtual void UnloadContent()
        {

        }

        void LevelLoaderRoutine(Object o)
        {
            LoadLevel();
            loading = false;
        }

        /// <summary>
        /// Called after loading is complete
        /// </summary>
        public abstract void SetUpLevel();
        public void CreateLevel(Archer player, ContentManager Content)
        {
            this.player = player;
            loading = true;
            setUp = false;
            PlayState.Camera.Zoom = 0.2f;
            this.Content = new ContentManager(Content.ServiceProvider, Content.RootDirectory);
            font = Content.Load<SpriteFont>("SpriteFont1");
            ThreadPool.QueueUserWorkItem(new WaitCallback(LevelLoaderRoutine));
        }

        public void UnloadLevel()
        {

            collectables.Clear();
            collideables.Clear();
            enemies.Clear();

            skybox.Clear();
            background.Clear();
            foreground.Clear();

            if(player != null)
            player.projectiles.Clear();

            UnloadContent();
            if (Content != null)
            {
                Content.Unload();
                Content.Dispose();
                Content = null;
            }
        }

        /// <summary>
        /// Sets the loading bar text (should only be used in CreateLevel()
        /// </summary>
        /// <param name="text"></param>
        public virtual void SetLoadingText(string text)
        {
            loadingText = text;
        }

        public void DoUpdate(GameTime gameTime)
        {
            prevKeyState = keyState;
            keyState = Keyboard.GetState();

            if (!loading)
            {
                if (setUp) {
                    Update(gameTime);
                } else
                {
                    SetUpLevel();
                    setUp = true;
                }
                    
            }
        }

        protected virtual void Update(GameTime gameTime)
        {
            collectables.ForEach(x => x.Update(gameTime));
            player.Update(gameTime);
            player.CollideLevel(collideables); //players Collide level updates sprite boundingPoly's so if it will not be the first collision call called, the updating code needs to be moved
            player.CollideCollectables(collectables);
            enemies.ForEach(x => x.Update(gameTime));
            enemies.ForEach(x => x.CollideLevel(collideables));
            player.projectiles.ForEach(x => x.Update(gameTime));
            player.projectiles.ForEach(x => x.CollideEnemy(enemies));
            player.projectiles.ForEach(x => x.CollideLevel(collideables));
            enemies.ForEach(x => x.CollidePlayer(player));
            if(!pauseTimer)
                timer += gameTime.ElapsedGameTime;

            for (int i = enemies.Count - 1; i >= 0; i--)
            {
                if (!enemies[i].active)
                    enemies.RemoveAt(i);
            }

            for (int i = collectables.Count - 1; i >= 0; i--)
            {
                if (!collectables[i].active)
                    collectables.RemoveAt(i);
            }

            //if (keyState.IsKeyDown(Keys.NumPad0))
            //{
            //    LevelManager.NextLevel();
            //}

            //if (keyState.IsKeyDown(Keys.R))
            //    player.Reset(300, 0);

            if (keyState.IsKeyDown(Keys.F2) && !prevKeyState.IsKeyDown(Keys.F2))
                debug = !debug;

            if (player.dead)
            {
                if (PlayState.Camera.Zoom < 1.4805481f)
                    PlayState.Camera.Zoom /= 0.99f;
                else
                {
                    Win.lose = true;
                    LevelManager.SetLevel(LevelManager.LevelCount()-1);
                }
            }
        }

        public virtual void DrawSkybox(SpriteBatch sb, int width, int height)
        {
            if (!loading)
            {
                skybox.ForEach(x => {
                    x.width = width;
                    x.height = height;
                    x.Draw(sb);
                });
            }
        }

        public virtual void DrawGUI(SpriteBatch sb, int width, int height)
        {
            MouseState m = Mouse.GetState();
            sb.DrawString(font, "Time " + timer.Minutes + ":" + timer.Seconds + "." + timer.Milliseconds, new Vector2(288, 16), Color.Yellow);

            LineBatch.drawCrossX(sb, m.X, m.Y, 10, Color.White, Color.White);
            LineBatch.drawCross(sb, m.X, m.Y, 10, Color.Red, Color.Red);

            if (loading)
            {
                sb.DrawString(font, loadingText, new Vector2(width/2f,height/2f) - font.MeasureString(loadingText)/2, Color.White);
            }

            Vector2 mPos = Vector2.Transform(m.Position.ToVector2(), PlayState.Camera.InverseTransform);

            if (debug)
                sb.DrawString(font, "X:" + mPos.X + " Y:" + mPos.Y, new Vector2(width - font.MeasureString("X:" + mPos.X + " Y:" + mPos.Y).X, 0), Color.SlateGray);
            //spriteBatch.Draw(LineBatch._empty_texture, new Rectangle((int)m.X - 3, (int)m.Y - 3, 6, 6), Color.Red);
        }

        public virtual void Draw(SpriteBatch sb)
        {
            if (loading)
            {
                //sb.DrawString(font, loadingText, Vector2.Zero, Color.White);
            }
            else
            {

                background.ForEach(x => x.Draw(sb));
                collectables.ForEach(x => x.Draw(sb));
                player.Draw(sb);
                collideables.ForEach(x =>
                    x.Draw(sb)
                    );
                enemies.ForEach(x => x.Draw(sb));
                foreground.ForEach(x => x.Draw(sb));

                if (debug)
                {
                    player.drawBB(sb, Color.Yellow);
                    collideables.ForEach(x =>
                    {
                        x.drawBB(sb, Color.Red);
                        if (x.boundingPoly != null)
                            LineBatch.drawPolygon12(sb, x.boundingPoly, Color.Orange);
                    });
                    enemies.ForEach(x => x.drawBB(sb, Color.Blue));
                    collectables.ForEach(x =>
                    {
                        x.drawBB(sb, Color.Red);
                    });



                }
            }
        }
    }
}
