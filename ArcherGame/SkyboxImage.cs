﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

using RC_Framework;
using Sprite3Extension;

namespace ArcherGame
{
    class SkyboxImage: StaticImage
    {

        public float widthOffset, heightOffset;

        public SkyboxImage(Texture2D tex, float xPos, float yPos, float widthOffset, float heightOffset,SpriteEffects flip = SpriteEffects.None) :base(tex,xPos,yPos,null,null,flip)
        {
            this.widthOffset = widthOffset;
            this.heightOffset = heightOffset;
        }

        public SkyboxImage(Texture2D tex, float xPos, float yPos, float widthOffset, float heightOffset, Color c, SpriteEffects flip = SpriteEffects.None)
            : this(tex, xPos, yPos, widthOffset, heightOffset, flip)
        {
            this.c = c;
        }

        public override void Draw(SpriteBatch sb)
        {
            sb.Draw(tex, destinationRectangle: new Rectangle((int)xPos, (int)yPos, (int)(width + widthOffset), (int)(height + heightOffset)), color: c, effects: flip);
        }
    }
}
