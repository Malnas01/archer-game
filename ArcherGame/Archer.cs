﻿using RC_Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

using Sprite3Extension;
using Microsoft.Xna.Framework.Audio;

namespace ArcherGame
{

    class Archer : Sprite3Parent
    {

        Vector2 offset;

        Sprite3 body;
        Vector2 bodyHS;
        Sprite3 backArm;
        Sprite3 bow;
        Vector2 bowHS;
        Vector2 bowOffsetFromBody;
        public Sprite3 legs;
        Vector2 legHS;

        double bodyAngleUpper = Math.PI / 5;
        double bodyAngleLower = Math.PI / 7;

        public float maxHealth = 100;
        public float health;

        public Vector2 oldVel;
        public Vector2 velocity;
        public Vector2 acceleration = new Vector2(2f,2f);
        public float jumpSpeed = 75;
        public float resistanceX = 0.07f;
        public float resistanceY = 0.018f;
        public float gravity = 3.5f;
        public bool grounded = false;
        public bool jumped = false;

        public bool drawing = false;
        public float powerDelta = 1.5f;
        public float power = 0;
        public float powerMax = 30;

        Rectangle legCollider;
        Rectangle bodyCollider;
        Rectangle headCollider;

        public List<Projectile> projectiles;

        public Sprite3 attatchedGround;
        public Vector2 groundAttachTexSpace;
        public Vector2 playerAttatchPointOffset;

        SoundEffect arrowSound;

        public bool inputDisabled = false;

        public bool dead = false;
        public bool knockBack = false;

        public Archer(ContentManager Content, Vector2 pos)
        {
            health = maxHealth;
            offset = new Vector2(0, 50);
            this.pos = pos;
            oldVel = Vector2.Zero;
            this.velocity = Vector2.Zero;

            Arrow.texture = Content.Load<Texture2D>("arrow2");
            arrowSound = Content.Load<SoundEffect>("swoosh");
            projectiles = new List<Projectile>();

            legs = new Sprite3(true, Content.Load<Texture2D>("legSprite"), pos.X, pos.Y);
            legs.setXframes(6);
            legs.setYframes(2);
            legs.setWidth(380);
            legs.setHeight(320);

            legs.addAnimation("Run", 1, 11);
            legs.addAnimation("RunBack", new Vector2[] { new Vector2(5, 1), new Vector2(4, 1), new Vector2(3, 1), new Vector2(2, 1), new Vector2(1, 1), new Vector2(0, 1), new Vector2(5, 0), new Vector2(4, 0), new Vector2(3, 0), new Vector2(2, 0), new Vector2(1, 0) });
            legs.addAnimation("Stand", 0, 1);

            body = new Sprite3(true, Content.Load<Texture2D>("mistbodyfront"), pos.X, pos.Y);
            bow = new Sprite3(true, Content.Load<Texture2D>("bowSprite2"), pos.X, pos.Y);
            bow.setXframes(3);
            bow.setYframes(2);
            bow.setWidth(768);
            bow.setHeight(448);
            

            bow.addAnimation("Hold", 1, 0).SetPlaying();
            bow.addAnimation("Draw", 1, 2);
            bow.addAnimation("Drawn", 2, 0);
            bow.addAnimation("Release",new Vector2[]{new Vector2(0,1),new Vector2(1,1), new Vector2(0,0)});

            backArm = new Sprite3(true, Content.Load<Texture2D>("arm"), pos.X, pos.Y);

            int offX = 203;
            int offY = 60;
            legHS = new Vector2(offX, offY);
            legs.setHSoffset(legHS);

            legCollider = new Rectangle( -150, 124 - offY, 281, 180);
            bodyCollider = new Rectangle(-200, offY - 246, 381, 250);
            headCollider = new Rectangle(-125, offY - 400, 231, 154);

            bb = new Rectangle(-100, -280, 181, 584);

            bodyHS = new Vector2(175, 345);
            body.setHSoffset(bodyHS);
            body.setPos(legs.getPos() + body.getHSOffset());

            bowHS = new Vector2(145, 233);
            bow.setHSoffset(bowHS);
            bowOffsetFromBody = new Vector2(122,175) - body.getHSOffset();

            backArm.setHSoffset(bow.getHSOffset());

        }

        public void NewGame() {
            health = maxHealth;
            this.pos = pos;
            oldVel = Vector2.Zero;
            velocity = Vector2.Zero;
            power = 0;
            grounded = false;
            jumped = false;
            inputDisabled = false;
            PlayState.Camera.offset = Vector2.Zero;
            attatchedGround = null;
            playerAttatchPointOffset = Vector2.Zero;

            projectiles = new List<Projectile>();

            body.setColor(Color.White);
            legs.setColor(Color.White);
            bow.setColor(Color.White);
            backArm.setColor(Color.White);

        }

        public void Reset(float x, float y)
        {
            Reset(new Vector2(x, y));
        }

        public void Reset(Vector2 pos)
        {
            this.pos = pos;
            this.setPos(pos);
            oldVel = Vector2.Zero;
            velocity = Vector2.Zero;
            power = 0;
            grounded = false;
            jumped = false;
            inputDisabled = false;
            PlayState.Camera.offset = Vector2.Zero;
            attatchedGround = null;
            playerAttatchPointOffset = Vector2.Zero;
        }

        protected void Shoot() {
            projectiles.Add(new Arrow(bow.getPosX(), bow.getPosY(), power * 2 + 10, body.flip == SpriteEffects.FlipHorizontally ? (bow.displayAngle + MathHelper.Pi)%MathHelper.TwoPi: bow.displayAngle));
            arrowSound.Play();
        }


        public Rectangle getBBOf(Rectangle collider)
        {
            Rectangle box = collider;
            box.X += (int)Math.Round(pos.X + offset.X);
            box.Y += (int)Math.Round(pos.Y + offset.Y);
            return box;
        }

        public bool Collides(Rectangle r)
        {
            if (!getBB().Intersects(r)) return false;
            if (getBBOf(legCollider).Intersects(r)) return true;
            if (getBBOf(bodyCollider).Intersects(r)) return true;
            if (getBBOf(headCollider).Intersects(r)) return true;
            return false;
        }

        public bool Collides(Polygon12 p)
        {
            if (!p.Intersects(getBB())) return false;
            if (p.Intersects(getBBOf(legCollider))) return true;
            if (p.Intersects(getBBOf(bodyCollider))) return true;
            if (p.Intersects(getBBOf(headCollider))) return true;
            return false;
        }

        public bool Collides(Line l)
        {
            if (!l.Intersects(getBB())) return false;
            if (l.Intersects(getBBOf(legCollider))) return true;
            if (l.Intersects(getBBOf(bodyCollider))) return true;
            if (l.Intersects(getBBOf(headCollider))) return true;
            return false;
        }

        public void Knockback(Vector2 vel)
        {
            if (vel != Vector2.Zero)
            {
                velocity = vel;
                jumped = true;
                knockBack = true;
            }
        }

        public bool Hurt(float amount)
        {
            return Hurt(amount, Vector2.Zero);
        }
        public bool Hurt(float amount, Vector2 knockbackVel)
        {
            if (!knockBack && !dead)
            {
                Knockback(knockbackVel);
                health -= amount;
                if (health <= 0)
                {
                    Die();
                    return true;
                }

            } 
            return false;
        }

        public void Die()
        {
            dead = true;
            if (!knockBack)
            {
                Knockback(new Vector2(0,-50));
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (attatchedGround != null)
            {
                pos = Sprite3Extension.Sprite3Extension.AttatchPoint(attatchedGround, groundAttachTexSpace) + playerAttatchPointOffset;
            }

            if (!inputDisabled && !dead)
            {

                //Get inputs
                MouseState m = Mouse.GetState();
                if (m.LeftButton == ButtonState.Pressed)
                {
                    //Start drawing bow
                    if (power <= 0)
                    {
                        power = 0;
                        drawing = true;
                    }
                    //Bow fully drawn
                    else if (power >= powerMax)
                    {
                        power = powerMax;
                    }
                    //Bow drawing
                    if (drawing)
                    {
                        Animation anim = bow.getAnimation("Draw");
                        int frame = (int)((anim.Length - 1) * power / powerMax);
                        bow.setXframe(anim.GetFrameXPos(frame));
                        bow.setYframe(anim.GetFrameYPos(frame));
                        power += powerDelta;
                    }
                    //Bow Releasing
                    else
                    {
                        power -= powerDelta;
                        Animation anim = bow.getAnimation("Release");
                        int frame = (int)Math.Round((anim.Length - 1) * (powerMax - power) / powerMax);
                        bow.setXframe(anim.GetFrameXPos(frame));
                        bow.setYframe(anim.GetFrameYPos(frame));
                    }
                }
                else
                {
                    //Bow Released
                    if (drawing)
                    {
                        drawing = false;
                        Shoot();
                        power = powerMax;
                    }
                    //Bow Redrawing
                    if (power > 0)
                    {
                        power -= powerDelta;
                        Animation anim = bow.getAnimation("Release");
                        int frame = (int)Math.Round((anim.Length - 1) * (powerMax - power) / powerMax);
                        bow.setXframe(anim.GetFrameXPos(frame));
                        bow.setYframe(anim.GetFrameYPos(frame));
                    }
                    //Bow Standby
                    else
                    {
                        power = 0;
                        bow.setXframe(1);
                    }
                }

                oldVel = velocity;
                if (!knockBack)
                {
                    KeyboardState k = Keyboard.GetState();
                    //Use inputs
                    if (k.IsKeyDown(Keys.W))
                    {
                        if (!jumped)
                        {
                            velocity.Y = -jumpSpeed;
                            jumped = true;
                        }
                    }
                    if (k.IsKeyDown(Keys.S))
                    {
                        velocity.Y += acceleration.Y;
                    }
                    if (k.IsKeyDown(Keys.A))
                    {
                        velocity.X -= acceleration.X;
                    }
                    if (k.IsKeyDown(Keys.D))
                    {
                        velocity.X += acceleration.X;
                    }
                }
            }

            if (dead && body.getColor().A > 100f)
            {
                Vector4 sub = Vector4.One * 0.006f;
                body.setColor(new Color(body.getColor().ToVector4() - sub));
                legs.setColor(new Color(legs.getColor().ToVector4() - sub));
                bow.setColor(new Color(bow.getColor().ToVector4() - sub));
                backArm.setColor(new Color(backArm.getColor().ToVector4() - sub));
            }

            //Do physics/movements
            setPos(pos + velocity);

            float velX = Math.Abs(velocity.X);
            float velY = Math.Abs(velocity.Y);
            velocity.X = Math.Max(velX - resistanceX * velX, 0) * Math.Sign(velocity.X);
            velocity.Y = Math.Max(velY - resistanceY * velY, 0) * Math.Sign(velocity.Y);

            velocity.Y += gravity;

            if (!dead)
                legs.animationTick();

            projectiles.ForEach(x => x.Update(gameTime));
            for (int i = projectiles.Count - 1; i >= 0; i--)
            {
                if (projectiles[i].active == false)
                    projectiles.RemoveAt(i);
            }

        }

        public void CollideCollectables(List<Collectable> collectables)
        {
            foreach (Collectable c in collectables)
            {
                if(getBB().Intersects(c.getBoundingBoxAA())) {
                    c.Collect(this);
                    if (collectables.Count == 0)
                        return;
                }
            }
        }

        Vector2 collisionPoint;
        public void CollideLevel(List<Sprite3> collideables)
        {

            grounded = false;
            attatchedGround = null;
            
            foreach (Sprite3 s in collideables)
            {

                if (s.boundingPolyOffsets != null)
                    s.computeBoundingPolyFromPos();

                Polygon12 p = s.boundingPoly;
                if(p != null)
                {
                    #region Bounding Poly Collision
                    Rectangle bodyBB = getBBOf(bodyCollider);
                    Line leftHand = Line.FromRectLeft(bodyBB);

                    if (leftHand.Intersects(p, true))
                    {
                        pos.X += 0.1f;
                        leftHand.AddX(0.1f);
                        while (leftHand.Intersects(p,true))
                        {
                            pos.X += 0.1f;
                            leftHand.AddX(0.1f);
                        }
                        if (velocity.X < 0) velocity.X = 0;
                    }
                    Line rightHand = Line.FromRectRight(bodyBB);
                    if (rightHand.Intersects(p, true))
                    {                 
                        pos.X -= 0.1f;
                        rightHand.AddX(-0.1f);
                        while (rightHand.Intersects(p, true))
                        {
                            pos.X -= 0.1f;
                            rightHand.AddX(-0.1f);
                        }
                        if (velocity.X > 0) velocity.X = 0;
                    }

                    Rectangle legBB = getBBOf(legCollider);
                    Line leftLeg = Line.FromRectLeft(legBB);
                    Line rightLeg = Line.FromRectRight(legBB);
                    Line middleLeg = Line.FromRectBottom(legBB);
                    if (leftLeg.Intersects(p, out collisionPoint, true) || rightLeg.Intersects(p, out collisionPoint, true) || middleLeg.Intersects(p, out collisionPoint, true))
                    {

                        float dist = gravity / 10;
                        pos.Y -= dist;
                        leftLeg.AddY(-dist);
                        middleLeg.AddY(-dist);
                        rightLeg.AddY(-dist);
                        if (velocity.Y >= -0.01)
                        {
                            velocity.Y = 0;
                            grounded = true;
                        }

                        while (dist > 0.001f && (leftLeg.Intersects(p, out collisionPoint, true) || rightLeg.Intersects(p, out collisionPoint, true) || middleLeg.Intersects(p, out collisionPoint, true)))
                        {
                            pos.Y -= dist;
                            leftLeg.AddY(-dist);
                            middleLeg.AddY(-dist);
                            rightLeg.AddY(-dist);
                        }
                        //Move the player slightly into the ground to hug really small inclines (so every second frame isn't in the air)
                        pos.Y += 1.5f;
                        leftLeg.AddY(1.5f);
                        middleLeg.AddY(1.5f);
                        rightLeg.AddY(1.5f);
                        
                        attatchedGround = s;
                        s.computeTexToWorldSpaceMatrix();
                        groundAttachTexSpace = Vector2.Transform(pos, s.WorldToTexSpaceMatrix);
                        if (collisionPoint != Vector2.Zero)
                            playerAttatchPointOffset = pos - collisionPoint;
                        else
                            playerAttatchPointOffset = Vector2.Zero;
                         

                    }

                    Line head = Line.FromRectTop(getBBOf(headCollider));
                    if (head.Intersects(p, true))
                    {
                        //pos.Y += Math.Abs(head.p1.Y - collisionPoint.Y);
                        
                        pos.Y += 0.1f;
                        head.AddY(0.1f);
                        while (head.Intersects(p, true))
                        {
                            pos.Y += 0.1f;
                            head.AddY(0.1f);
                        }
                    }
                    #endregion
                }
                else
                {
                    #region AABB Collision
                    Rectangle sBB = s.getBoundingBoxAA();
                    Rectangle BB = getBBOf(bodyCollider);

                    //Body collision
                    if (BB.Intersects(sBB))
                    {
                        if (sBB.Center.X < pos.X)
                        {
                            
                            if (velocity.X <= 0)
                            {
                                velocity.X = 0;
 
                            }
                            pos.X += sBB.Right - BB.Left - 1;
                        }
                        else if (pos.X < sBB.Center.X)
                        {
                            
                            if (velocity.X >= 0)
                            {
                                velocity.X = 0;
 
                            }
                            pos.X += sBB.Left - BB.Right + 1;
                        }
                    
                    }

                    BB = getBBOf(legCollider);
                    if (BB.Intersects(sBB))
                    {
                        if (velocity.Y >= -0.01)
                        {
                            velocity.Y = 0;
                            grounded = true;
                        }
                        pos.Y += sBB.Top - BB.Bottom + 1;

                        attatchedGround = s;
                        s.computeTexToWorldSpaceMatrix();
                        groundAttachTexSpace = Vector2.Transform(pos,s.WorldToTexSpaceMatrix);
                        playerAttatchPointOffset = Vector2.Zero;
                    }

                    BB = getBBOf(headCollider);
                    if (BB.Intersects(sBB))
                    {
                        if (velocity.Y < 0)
                            velocity.Y = 0;
                        pos.Y += sBB.Bottom - BB.Top - 1;
                    }
                    #endregion
                }
            }
            if (grounded)
            {
                jumped = false;
                knockBack = false;
            }
            else
            {
            }
            UpdateSpritePos();
        }

        public void UpdateSpritePos()
        {
            //Get inputs
            MouseState mState = Mouse.GetState();
            Vector2 m = new Vector2(mState.X, mState.Y);
            m = Vector2.Transform(m, PlayState.Camera.InverseTransform);

            //Move all sprites

            legs.setPos(pos + offset);
            body.attatchTo(legs);

            double bowAngle;
            double bodyAngle;
            //bodyAngleLimit = Math.PI / 5;

            if (dead)
            {
                bowAngle = bow.displayAngle;
                bodyAngle = body.displayAngle;
            }
            else
            {
                bowAngle = Math.Atan2(m.Y - bow.getPosY(), m.X - bow.getPosX()); //Get angle from bow to mouse 
                bodyAngle = Math.Atan2(m.Y - body.getPosY(), m.X - body.getPosX()); //Get angle from body to mouse
            }
            //Flip the character accordingly
            if (m.X < body.getPosX())
            {
                body.flip = SpriteEffects.FlipHorizontally;
                bodyAngle -= Math.PI * Math.Sign(bodyAngle);

                bowAngle += Math.PI * Math.Sign(bowAngle);
            }
            else
            {
                body.flip = SpriteEffects.None;
            }
            bow.flip = body.flip;
            backArm.flip = bow.flip;
            legs.flip = body.flip;

            
            if (body.flip == SpriteEffects.FlipHorizontally)
                body.setHSoffset(new Vector2(body.getWidth() - bodyHS.X, bodyHS.Y));
            else
                body.setHSoffset(bodyHS);
            
            if (legs.flip == SpriteEffects.FlipHorizontally)
                legs.setHSoffset(new Vector2(legs.getWidth() - legHS.X, legHS.Y));
            else
                legs.setHSoffset(legHS);
            
            if (Keyboard.GetState().IsKeyDown(Keys.P))
            {

            }

            double angleLimit;

            if (bodyAngle > 0 ^ body.flip == SpriteEffects.FlipHorizontally)
                angleLimit = bodyAngleLower;
            else
                angleLimit = bodyAngleUpper;

            bodyAngle = Math.Max(Math.Abs(bodyAngle) - angleLimit, 0) * Math.Sign(bodyAngle);
            body.displayAngle = (float)bodyAngle; //Set body display angle

            //Attatch bow to body
            bow.attatchTo(body, bowOffsetFromBody + bodyHS);
            bow.displayAngle = (float)(bowAngle); //Set bow display angle
            if (bow.flip == SpriteEffects.FlipHorizontally)
                bow.setHSoffset(new Vector2(bow.getWidth() - bowHS.X, bowHS.Y));
            else
                bow.setHSoffset(bowHS);

            //Attatch back arm to bow
            backArm.setPos(bow.getPos());
            //backArm.attatchTo(bow);
            backArm.displayAngle = bow.displayAngle; //Set back arm display angle to same as bow
            backArm.setHSoffset(bow.getHSOffset());

            if ((Util.aequal(velocity.X, 0f, 0.001f) || Math.Abs(velocity.X) - Math.Abs(oldVel.X) < 0) && grounded)
            {
                legs.getAnimation("Stand").SetPlaying();
                //legs.ticks = -1;
            }
            else
            {
                if (legs.getXframe() == 0 && legs.getYframe() == 0)
                    legs.ticks = -1;
                if (velocity.X > 0 ^ legs.flip == SpriteEffects.FlipHorizontally ^ velocity.Y > 50)
                    legs.getAnimation("Run").SetPlaying();
                else
                    legs.getAnimation("RunBack").SetPlaying();

                if (grounded)
                    legs.ticksBetweenFrames = (int)(19 - 16 * Math.Abs(velocity.X) / 26);
                else
                {
                    if (velocity.Y < 80)
                        legs.ticksBetweenFrames = (int)(20 - 16 * Math.Abs(velocity.X) / 26 + 4 * (80 + velocity.Y) / 80);
                    else
                        legs.ticksBetweenFrames = (int)(10 - 8 * Math.Abs(velocity.Y) / 200);
                }
            }
                


        }

        public override void Draw(SpriteBatch spriteBatch)
        {
          /*  if (body.flip == SpriteEffects.FlipHorizontally)
            {
                projectiles.ForEach(x => x.Draw(spriteBatch));
                bow.draw(spriteBatch);
                
                legs.draw(spriteBatch);
                body.draw(spriteBatch);
                backArm.draw(spriteBatch);
            }
            else
            {*/                

            backArm.draw(spriteBatch);
            legs.draw(spriteBatch);
            body.draw(spriteBatch);
            bow.draw(spriteBatch);
            projectiles.ForEach(x => x.Draw(spriteBatch));
            //}

            
        }

        public Rectangle getBB()
        {
            Rectangle box = bb;
            box.X += (int)Math.Round(pos.X);
            box.Y += (int)Math.Round(pos.Y);
            return box;
        }

        public Rectangle oldBB()
        {
            Rectangle box = bb;
            box.X += (int)Math.Round(oldPos.X);
            box.Y += (int)Math.Round(oldPos.Y);
            return box;
        }
        public virtual void drawBB(SpriteBatch sb, Color c) // 
        {
            LineBatch.drawLineRectangle(sb, getBB(), Color.Red);

            LineBatch.drawLineRectangle(sb, getBBOf(legCollider), c);
            LineBatch.drawLineRectangle(sb, getBBOf(bodyCollider), c);
            LineBatch.drawLineRectangle(sb, getBBOf(headCollider), c);

            Line leftLeg = Line.FromRectLeft(getBBOf(legCollider));
            Line rightLeg = Line.FromRectRight(getBBOf(legCollider));
            LineBatch.drawLine(sb, Color.Purple, leftLeg.p1, leftLeg.p2);
            LineBatch.drawLine(sb, Color.Purple, rightLeg.p1, rightLeg.p2);

            legs.drawHS(sb, Color.Green);
            body.drawHS(sb, Color.Violet);

            LineBatch.drawCrossX(sb, collisionPoint.X, collisionPoint.Y, 20, Color.Yellow, Color.Yellow);

            projectiles.ForEach(x => x.drawBB(sb,Color.GhostWhite));

        }
    }
}
