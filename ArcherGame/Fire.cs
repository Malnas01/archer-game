﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using RC_Framework;
using Sprite3Extension;
namespace ArcherGame
{
    class Fire : Collectable
    {
        public float dmg = 1f;
        public Fire(Texture2D tex, float x, float y, float width, float height,int xFrames = 7, int yFrames = 7, int frames = 48)
            : base(x,y)
        {
            setTexture(tex,true);
            setXframes(xFrames);
            setYframes(yFrames);
            setWidthHeight(width, height);
            setHSoffset(new Vector2(0, (float)tex.Height/yFrames));
            this.addAnimation("Animate", 0, frames).SetPlaying();
            setBB(40, tex.Height / yFrames / 2, tex.Width / xFrames - 80, tex.Height / yFrames / 2);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            animationTick();
            tick();
        }

        public override void Draw(SpriteBatch sb)
        {
            base.Draw(sb);
        }

        public override void Collect(Archer player)
        {
            player.Hurt(dmg);
        }
    }
}
