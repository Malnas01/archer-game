﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

using RC_Framework;
using Sprite3Extension;
namespace ArcherGame
{
    abstract class  Enemy : Sprite3
    {
        protected float health = 100;

        public Enemy()
            : base()
        {
            
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            tick();
        }

        public abstract bool Collides(Rectangle r);
        public abstract bool Collides(Polygon12 p);
        public abstract bool Collides(Line l);

        public virtual void CollideLevel(List<Sprite3> collideables)
        {

        }

        public virtual void CollidePlayer(Archer player)
        {

        }


        public virtual bool Hurt(float amount, Sprite3 source = null)
        {
            health -= amount;
            if (health <= 0)
            {
                Die();
                return true;
            }
            return false;
        }

        public virtual void Die()
        {
            this.active = false;
        }

    }
}
