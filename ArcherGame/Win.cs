﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.IO;

using RC_Framework;
using Sprite3Extension;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace ArcherGame
{
    class Win : Level
    {
        Textbox textBox;
        List<SaveData> scores;
        Portal portal;

        SaveData playerScore;
        bool scoreAdded = false;

        int MaxScores = 10;
        int scoresX = 1700;
        int scoresY = -3000;
        int scoreNameWidth = 1536;
        int scoreTimeWidth = 750;
        int scoreEntryHeight = 192;

        MouseState prevMouseState;
        Action OnEndGame;

        public static bool lose;

        const string SAVE_FILE = "highscores.dat";

        
        [Serializable]
        class SaveData : ISerializable
        {
            public long time;
            public string name;
            public SaveData(long time, string name)
            {
                this.time = time;
                this.name = name;
            }

            public SaveData(SerializationInfo info, StreamingContext ctxt)
            {
                this.time = (long)info.GetValue("time", typeof(long));
                this.name = (string)info.GetValue("name", typeof(string));
            }

            public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
            {
                info.AddValue("time", this.time);
                info.AddValue("name", this.name);
            }

            public override string ToString()
            {
                return name + " " +time;
            }
        }

        public void SaveScores()
        { 
            try {
                Stream file = File.Open(SAVE_FILE, FileMode.Create);
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(file, scores);
                file.Close();
            } catch (Exception e)
            {
                Console.WriteLine("Unable to save: " + e.Message);
            }
        }

        public void AddScore() {
            if (scoreAdded)
            {
                playerScore.name = textBox.Text;
            } else if (scores.Count < MaxScores || timer.Ticks < scores.Last().time)
            {
                scores.Add(playerScore = new SaveData(timer.Ticks, textBox.Text));
                scores = scores.OrderBy(a => a.time).ToList();
                while (scores.Count > MaxScores)
                    scores.RemoveAt(scores.Count - 1);
                scoreAdded = true;
            }
        }

        public Win(Action OnEndGame)
        {
            pauseTimer = true;
            this.OnEndGame = OnEndGame;
        }

        public override void LoadLevel()
        {
            SetLoadingText("Loading Level");
            Stream file = File.Open(SAVE_FILE, FileMode.OpenOrCreate);

            if (file.Length > 0)
            {
                try
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    scores = (List<SaveData>)bf.Deserialize(file);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Unable to load: " + e.Message);
                    scores = new List<SaveData>();
                }
            }
            else
                scores = new List<SaveData>();
            file.Close();
            scores = scores.OrderBy(a => a.time).ToList();

            //enemies.Add(new Ghost(3000,0,Content,player));
            //aenemies.Add(new Bat(1000, -1000, Content, player));
            float scale = 3f;
            textBox = new Textbox(new Rectangle(2000, -1000, (int)(512 * scale), (int)(64 * scale)), Content, "Enter Name");
            textBox.boxColor = Color.Black;
            textBox.textColor = Color.AntiqueWhite;
            skybox.Add(new StaticImage(Content.Load<Texture2D>("Sky"), 0, 0, (float)Game1.window.ClientBounds.Width, (float)Game1.window.ClientBounds.Height));
            background.Add(new StaticImage(Content.Load<Texture2D>("mountains"), -4000, -1600, 3881 * scale, 1984 * scale));

            Sprite3 platform;
            collideables.Add(LevelHelper.LongPlatorm(Content.Load<Texture2D>("platform2"), 0, 300 + 46 * scale,scale));

            //Thread.Sleep(1000);

            collideables.Add(platform = LevelHelper.PillarStraight(Content.Load<Texture2D>("pillar"), 46 * scale, 300 + 46 * scale,scale));
            platform.setHSoffset(new Vector2(0, 571));

            collideables.Add(platform = LevelHelper.PillarStraight(Content.Load<Texture2D>("pillar"), 1651 * scale, 300 + 31 * scale, scale));
            platform.setHSoffset(new Vector2(172, 571));

            //collectables.Add(new ArrowHead(1651 * scale - 2000, 31 * scale - 50, Content));
            collectables.Add(portal = new Portal(1651 * scale - 800, 31 * scale - 50, LevelManager, Content));
            portal.OnCollect = delegate(Archer player)
            {
                if(!lose)
                    SaveScores();
                lose = false;
                if (OnEndGame != null)
                    OnEndGame();
            };
            if(lose)
                portal.visible = true;
            else 
                portal.visible = false;
            playerScore = new SaveData(timer.Ticks, "");
            prevMouseState = Mouse.GetState();

            textBox.OnLoseFocus = delegate()
            {
                if (textBox.Text.Length > 0)
                {
                    AddScore();
                    portal.visible = true;
                }
                else
                    portal.visible = false;
            };

        }

        public override void SetUpLevel()
        {
            scoreAdded = false;
            player.Reset(new Vector2(1000, -200));
            player.dead = false;
            if (player.health <= 0)
                player.health = 1;
            PlayState.Camera.offset = new Vector2(0, 1000);
            
        }

        public override void UnloadContent()
        {
            lose = false;
            base.UnloadContent();
            textBox = null;
            scores = null;
            portal = null;
            playerScore = null;
        }

        protected override void Update(GameTime gameTime)
        {
            if (!lose)
            {
                MouseState m = Mouse.GetState();
                if (m.LeftButton == ButtonState.Released && prevMouseState.LeftButton == ButtonState.Pressed)
                    if (textBox.rect.Contains(Vector2.Transform(m.Position.ToVector2(), PlayState.Camera.InverseTransform)))
                        textBox.Focus = true;
                    else
                        textBox.Focus = false;

                player.inputDisabled = textBox.Focus;

                textBox.Update();
                prevMouseState = m;
            }
            base.Update(gameTime);
        }




        public override void Draw(SpriteBatch sb)
        {
            base.Draw(sb);
            if (!loading)
            {

                for (int i = 0; i < scores.Count; i++)
                {
                    Textbox.DrawTextBox(sb, new Rectangle(scoresX, scoresY + scoreEntryHeight * i, scoreNameWidth, scoreEntryHeight), scores[i].name, Color.Wheat);
                    TimeSpan ts = TimeSpan.FromTicks(scores[i].time);
                    Textbox.DrawTextBox(sb, new Rectangle(scoresX + scoreNameWidth, scoresY + scoreEntryHeight * i, scoreTimeWidth, scoreEntryHeight), ts.Minutes + ":" + ts.Seconds + "." + ts.Milliseconds, Color.Yellow);
                }


                if (!lose)
                {
                    textBox.Draw(sb);
                    sb.DrawString(font, "Time " + timer.Minutes + ":" + timer.Seconds + "." + timer.Milliseconds, new Vector2(textBox.rect.X + 100, textBox.rect.Y + textBox.rect.Height), Color.Yellow, 0, Vector2.Zero, 10, SpriteEffects.None, 0);
                }
            }
        }

    }
}
