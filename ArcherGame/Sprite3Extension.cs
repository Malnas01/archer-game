﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RC_Framework;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Sprite3Extension
{
    static class Sprite3Extension
    {
        public static void AnimationTick(this RC_RenderableList list)
        {
            for (int i = 0; i < list.Count(); i++)
            {
                RC_Renderable r = list.getRenderable(i);
                if(r is Sprite3) {
                    (r as Sprite3).animationTick();
                }
            }
        }

        static Dictionary<Sprite3, Dictionary<string, Animation>> animationDictionary = new Dictionary<Sprite3, Dictionary<string, Animation>>();

        public static Animation addAnimation(this RC_Framework.Sprite3 sprite, string name, int startFrame, int frames, int ticksBetweenFrames = 5)
        {
            if (!animationDictionary.ContainsKey(sprite))
                animationDictionary.Add(sprite, new Dictionary<string, Animation>());

            Animation a = new Animation(sprite, startFrame, frames, ticksBetweenFrames);
            animationDictionary[sprite].Add(name, a);
            return a;
        }

        public static Animation addAnimation(this RC_Framework.Sprite3 sprite, string name, Vector2[] animationSequence, int ticksBetweenFrames = 5)
        {
            if (!animationDictionary.ContainsKey(sprite))
                animationDictionary.Add(sprite, new Dictionary<string, Animation>());

            Animation a = new Animation(sprite, animationSequence, ticksBetweenFrames);
            animationDictionary[sprite].Add(name, a);
            return a;
        }

        public static Animation getAnimation(this Sprite3 sprite, string name)
        {
            return animationDictionary[sprite][name];
        }

        public static void playAnimation(this Sprite3 sprite, string name)
        {
            animationDictionary[sprite][name].SetPlaying();
        }

        public static void attatchTo(this Sprite3 me, Sprite3 sprite)
        {
            me.attatchTo(sprite, sprite.getHSOffset());
        }

        public static Vector2 AttatchPoint(Sprite3 sprite, Vector2 offset)
        {
            sprite.computeTexToWorldSpaceMatrix();
            return Vector2.Transform(offset, sprite.TexToWorldSpaceMatrix);
            /*
            if (sprite.flip == SpriteEffects.FlipHorizontally)
                offset.X = -offset.X;
            //else if (sprite.flip == SpriteEffects.FlipVertically)
            //    offset.Y = -offset.Y;

            double x = sprite.getPosX();
            double y = sprite.getPosY();

            if (offset == Vector2.Zero)
            {
                return sprite.getPos();
            }

            double angle = Math.Atan2(offset.Y, offset.X);
            double length = offset.Length();

            x += Math.Cos(sprite.displayAngle + angle) * length;
            y += Math.Sin(sprite.displayAngle + angle) * length;

            return new Vector2((float)x, (float)y); */
        }

        /// <summary>
        /// Positions this sprite at the same offset in texture space regardless of rotation
        /// </summary>
        /// <param name="me"></param>
        /// <param name="sprite"></param>
        /// <param name="offset"></param>
        public static void attatchTo(this Sprite3 me, Sprite3 sprite, Vector2 offset)
        {


            sprite.computeTexToWorldSpaceMatrix();
            me.setPos(Vector2.Transform(offset, sprite.TexToWorldSpaceMatrix));
            return;
            /*if (sprite.flip == SpriteEffects.FlipHorizontally)
                offset.X = -offset.X;
            else if (sprite.flip == SpriteEffects.FlipVertically)
                offset.Y = -offset.Y;

            double x = sprite.getPosX();
            double y = sprite.getPosY();

            if (offset == Vector2.Zero)
            {
                me.setPos(sprite.getPos());
                return;
            }

            double angle = Math.Atan2(offset.Y, offset.X);
            double length = offset.Length();

            x += Math.Cos(sprite.displayAngle + angle) * length;
            y += Math.Sin(sprite.displayAngle + angle) * length;

            me.setPos((float)x, (float)y); */
        }

        /// <summary>
        /// Positions this sprite at the same offset in texture space regardless of rotation and matches rotation
        /// </summary>
        /// <param name="me"></param>
        /// <param name="sprite"></param>
        /// <param name="offset"></param>
        /// <param name="rotationOffset"></param>
        public static void attatchTo(this Sprite3 me, Sprite3 sprite, Vector2 offset, float rotationOffset)
        {
            me.attatchTo(sprite, offset);
            me.displayAngle = sprite.displayAngle + rotationOffset;
        }

        public static bool Collides(this Sprite3 me, Rectangle r) {
            if (me.boundingPoly == null)
            {
                return r.Intersects(me.getBoundingBoxAA());
            }
            else
            {
                return me.boundingPoly.Intersects(r);
            }
        }

        public static bool Collides(this Sprite3 me, Line l)
        {
            if (me.boundingPoly == null)
            {
                return l.Intersects(me.getBoundingBoxAA());
            }
            else
            {
                return me.boundingPoly.Intersects(l);
            }
        }

        public static bool Collides(this Sprite3 me, Vector2 point)
        {
            if (me.boundingPoly == null)
            {
                return me.getBoundingBoxAA().Contains(point);
            }
            else
            {
                return me.boundingPoly.Contains(point);
            }
        }


        public static void drawDebugAngle(this Sprite3 me, SpriteBatch spriteBatch)
        {
            LineBatch.drawLine(spriteBatch, me.getPosX(), me.getPosY(), me.getPosX() + (float)Math.Cos(me.displayAngle) * 50, me.getPosY() + (float)Math.Sin(me.displayAngle) * 50, Color.White);
        }

    }

    class Animation
    {

        int start;
        int end;
        Sprite3 sprite;
        int ticks;
        Vector2[] animationSequence = null;

        public Animation(Sprite3 sprite, int start, int frames, int ticksBetweenFrames = 5)
        {
            this.sprite = sprite;
            this.start = start;
            this.end = start + frames - 1;
            this.ticks = ticksBetweenFrames;
            animationSequence = null;
        }

        public Animation(Sprite3 sprite, Vector2[] animationSequence, int ticksBetweenFrames = 5)
        {
            this.sprite = sprite;
            this.start = 0;
            this.end = animationSequence.Length-1;
            this.animationSequence = animationSequence;
            this.ticks = ticksBetweenFrames;
        }

        public void SetPlaying()
        {
            sprite.setAnimationSequence(animationSequence, start, end, ticks);
        }
        public void SetPlaying(int speed)
        {
            sprite.setAnimationSequence(animationSequence, start, end, speed);
        }

        public int GetFrameXPos(int frame)
        {
            if (animationSequence == null)
                return start + frame;
            return (int)animationSequence[frame].X;
        }
        public int GetFrameYPos(int frame)
        {
            if (animationSequence == null)
                return 0;

            return (int)animationSequence[frame].Y;
        }

        public int Length
        {
            get {
                if (animationSequence != null)
                    return animationSequence.Length;
                else
                    return end - start + 1;
            }
        }

    }

}
