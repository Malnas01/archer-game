﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

using RC_Framework;
using Sprite3Extension;
using System.Threading;

namespace ArcherGame
{
    class Level1 : Level
    {

        Rectangle fallTrigger;
        Sprite3 fallingPillar;
        bool fall;
        Portal portal;

        public override void LoadLevel()
        {
            Sprite3 s;
            timer = new TimeSpan(); //Reset timer

            SetLoadingText("Loading Textures");
            #region Load Textures
            Texture2D stairs = Content.Load<Texture2D>("stairs");
            Texture2D floatingPlatform = Content.Load<Texture2D>("floatingPlatform");
            Texture2D longPlatform = Content.Load<Texture2D>("platform2");
            Texture2D pillar = Content.Load<Texture2D>("pillar");
            Texture2D grass = Content.Load<Texture2D>("grass");
            Texture2D wall = Content.Load<Texture2D>("wall");
            Texture2D texs = Content.Load<Texture2D>("floatingPlatform");
            Texture2D fire = Content.Load<Texture2D>("flames");
            #endregion

            SetLoadingText("Loading Backdrop");
            #region Backdrop
            skybox.Add(new SkyboxImage(Content.Load<Texture2D>("Sky"), 0, 0, 0, 0));
            skybox.Add(new SkyboxImage(Content.Load<Texture2D>("mountains"), -15, 300, 40, 0));
            background.Add(new StaticImage(Content.Load<Texture2D>("mountains"), -2500, 0, 25000, 10000,Color.LightGray, SpriteEffects.FlipHorizontally));
            background.Add(new StaticImage(pillar, 5750, 1540, 440, 3000 - 1540, new Color(0.2f, 0.2f, 0.2f)));
            background.Add(new StaticImage(pillar, 9100, 1540, 440, 3000 - 1540, new Color(0.2f, 0.2f, 0.2f)));
            background.Add(new StaticImage(pillar, 10820, 750, 440, 3000 - 750, new Color(0.2f, 0.2f, 0.2f)));
            background.Add(new StaticImage(pillar, 13622, -1925, 440, 3000 + 1925, new Color(0.2f, 0.2f, 0.2f)));
            #endregion

            SetLoadingText("Generating Platforms");
            #region Platforms

            collideables.Add(LevelHelper.Stairs(stairs, -200, 3000, 3));
            
            collideables.Add(s= LevelHelper.FloatingPlatform(floatingPlatform, 2000, 2500));
            s.flip = SpriteEffects.FlipVertically;

            collideables.Add(LevelHelper.FloatingPlatform(floatingPlatform, 3600, 1800));            
            
            collideables.Add(LevelHelper.LongPlatorm(longPlatform,5200,1100,3));

            collideables.Add(s = LevelHelper.LongPlatorm(Content.Load<Texture2D>("platform"), 10703, 1000, 3));
            s.displayAngle = (float)-MathHelper.PiOver4;
            
            collideables.Add(s = LevelHelper.Stairs(stairs, 20000, -4520, 3));
            s.displayAngle = MathHelper.PiOver2;

            collideables.Add(LevelHelper.LongPlatorm(longPlatform, 16600, -3000, 3));

            #endregion
            #region Falling Pillar
            fallingPillar = new Sprite3(true, pillar, 14075, -2421);
            fallingPillar.setHSoffset(new Vector2(86, 571));
            fallingPillar.setHeight(3000);
            fallingPillar.setWidth(400);
            collideables.Add(fallingPillar);
            fallingPillar.boundingPolyOffsets = new Polygon12(0);
            fallingPillar.boundingPolyOffsets.addPoint(new Vector2(0, 570));
            fallingPillar.boundingPolyOffsets.addPoint(new Vector2(0, 548));
            fallingPillar.boundingPolyOffsets.addPoint(new Vector2(30, 526));
            fallingPillar.boundingPolyOffsets.addPoint(new Vector2(30, 58));
            fallingPillar.boundingPolyOffsets.addPoint(new Vector2(0, 40));
            fallingPillar.boundingPolyOffsets.addPoint(new Vector2(0, 17));
            fallingPillar.boundingPolyOffsets.addPoint(new Vector2(165, 17));
            fallingPillar.boundingPolyOffsets.addPoint(new Vector2(165, 40));
            fallingPillar.boundingPolyOffsets.addPoint(new Vector2(136, 59));
            fallingPillar.boundingPolyOffsets.addPoint(new Vector2(136, 536));
            fallingPillar.boundingPolyOffsets.addPoint(new Vector2(165, 545));
            fallingPillar.boundingPolyOffsets.addPoint(new Vector2(165, 570));
            fallingPillar.displayAngle = 0;
            fall = false;

            fallTrigger = new Rectangle(13690, -2800, 500, 500);
            #endregion

            SetLoadingText("Spawning Monsters");
            #region Spawn Monsters
            enemies.Add(new Bat(2500, 2000, Content, player));
            enemies.Add(new Ghost(3600, 1400, Content, player, 3));
            enemies.Add(new Bat(8044, 2186, Content, player));
            enemies.Add(new Ghost(15573, -3390, Content, player, 3));
            enemies.Add(new Bat(10000, 200, Content, player));
            #endregion

            SetLoadingText("Spawning Loot");
            #region Pickups
            collectables.Add(new ArrowHead(2577, 2200, Content));
            collectables.Add(new ArrowHead(5384, 2551, Content));
            collectables.Add(new ArrowHead(10300, 1530, Content));
            collectables.Add(new ArrowHead(12064, -900, Content));
            collectables.Add(new ArrowHead(15242, -3373, Content));
            collectables.Add(new ArrowHead(17300, -3400, Content));
            //collectables.Add(new Fire(fire, 1500, 3000, 2000, 800, 7, 7));
            #endregion

            SetLoadingText("Finalizing");
            #region Level Bounds

            Sprite3 ground = LevelHelper.Ground(LineBatch._empty_texture, -8192, 3000, 36384, 5000, Color.Black);
            collideables.Add(ground); 

            LevelHelper.AddGrass(ground, foreground, grass, 1f, 1000, 1, 0.5f);

            collideables.Add(LevelHelper.Wall(wall, 0, 3000, 4));
            collideables.Add(LevelHelper.Wall(wall, 20000, 3000, 4, SpriteEffects.FlipHorizontally));
            #endregion
            portal = new Portal(19500, -3450, LevelManager, Content);

            #region NIU
            /*
             Sprite3 platform;
            platform = new Sprite3(true, Content.Load<Texture2D>("platform2"), 400, 265);
            platform.setHSoffset(new Vector2(956, 122));
            platform.boundingPolyOffsets = new Polygon12(4);
            platform.boundingPolyOffsets.point[0] = new Vector2(46, 46);
            platform.boundingPolyOffsets.point[1] = new Vector2(1651, 31);
            platform.boundingPolyOffsets.point[2] = new Vector2(1491, 200);
            platform.boundingPolyOffsets.point[3] = new Vector2(223, 207);
            platform.displayAngle = Util.degToRad(45);
            collidables.Add(platform);

            SetLoadingText("Loading Wall");
            Sprite3 wall = new Sprite3(true, Content.Load<Texture2D>("ground"), 600, 100);
            wall.displayAngle = MathHelper.PiOver2+0.1f;
            wall.setBoundingPolyFromBB();
            //wall.setBBToTexture();
            collidables.Add(wall);
            Thread.Sleep(150);

            collidables.Add(movingPlatform = new Sprite3(true, Content.Load<Texture2D>("ground"), 700, 100));
            movingPlatform.setDeltaSpeed(new Vector2(3,0));

            SetLoadingText("Loading Slope");
            slope  = new Sprite3(true, Content.Load<Texture2D>("ground"), 300, 100);
            slope.setHSoffset(new Vector2(128, 32));
            slope.displayAngle = -MathHelper.PiOver4*3;
            slope.setBBToTexture();
            slope.setBoundingPolyFromBB();
            collidables.Add(slope);
            Thread.Sleep(200);

            skybox.Add(new StaticImage(Content.Load<Texture2D>("Sky"),0, 0, (float)Game1.window.ClientBounds.Width, (float)Game1.window.ClientBounds.Height));
            
            enemies.Add(new Bat(500,100,Content,player));
            */
            #endregion
        }

        public override void SetUpLevel()
        {
            player.Reset(new Vector2(450, 2000));
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (fall)
            {
                if (fallingPillar.displayAngle < 1.287f)
                {
                    fallingPillar.displayAngle += 0.001f + 0.02f * fallingPillar.displayAngle;
                }
                else
                {
                    fallingPillar.displayAngle = 1.287f;
                }
            } else if(fallTrigger.Contains(player.getPos()))
                    fall = true;

            if (ArrowHead.CountIn(collectables) == 0)
                collectables.Add(portal);
        }

        public override void Draw(SpriteBatch sb)
        {
            base.Draw(sb);
            if (!loading)
            {
            }
        }

        public override void DrawGUI(SpriteBatch sb, int width, int height)
        {
            LevelHelper.DrawGround(sb, width, height);
            base.DrawGUI(sb, width, height);
        }

    }
}
