﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;



#pragma warning disable 1591 //sadly not yet fully commented

namespace RC_Framework
{
    public class ColorTicker
    {

        Color finalColour;
        Color initColour;
        int fadeTicks;
        public int loop; // 0=end (stay final colour), 1=Loop, 2=reverse
        int ticks;
        bool reverse;
        float lerp;
        Color curColour;
        
        public ColorTicker()
        {
            ticks = 0;
            fadeTicks = 10;        
            finalColour = Color.Black;
            initColour = Color.White;        
            reverse = false;
            lerp = 0; 
            curColour = initColour;
            loop = 0;
        }

        public ColorTicker(ColorTicker c)
        {
        finalColour = c.finalColour;
        initColour = c.initColour;
        ticks = c.ticks;
        fadeTicks = c.fadeTicks;
        loop = c.loop; // 0=end (stay final colour), 1=Loop, 2=reverse
        reverse = c.reverse;
        lerp = c.lerp;
        curColour = c.curColour;
        }

        public ColorTicker(Color fromColour, Color toColour, int fadeTicksQ)
        {
            finalColour = toColour;
            initColour = fromColour;
            ticks = 0;
            fadeTicks = fadeTicksQ;
            loop = 0; // 0=end (stay final colour), 1=Loop, 2=reverse
            reverse = false;
            lerp = 0;
            curColour = initColour;
        }

        public ColorTicker(Color fromColour, Color toColour, float secondsQ, int ticksPerSecond)
        {
            finalColour = toColour;
            initColour = fromColour;
            ticks = 0;
            fadeTicks = (int)(secondsQ * ticksPerSecond);
            loop = 0; // 0=end (stay final colour), 1=Loop, 2=reverse
            reverse = false;
            lerp = 0;
            curColour = initColour;
        }

        public bool finished()
        {
            if (ticks > fadeTicks) return true;
            return false;
        }

        public void reset()
        {
            ticks = 0;
            curColour = initColour;
            reverse = false;
        }

        public Color currColor()
        {
            return curColour;
        }

        public Color currColorInverse()
        {
            lerp = (float)ticks / (float)fadeTicks;
            if (!reverse) lerp = 1 - lerp;

            Color curColourI = Color.Lerp(initColour, finalColour, lerp);
            return curColourI;
        }

        public void Update()
        {
            ticks++;
            if (ticks > fadeTicks)
            {
                if (loop == 0)
                {
                    return;
                }
                if (loop == 1)
                {
                    ticks = 0;
                    return;
                }
                if (loop == 2)
                {
                    ticks = 0;
                    reverse = !reverse;
                    return;
                }


            }
            lerp = (float)ticks / (float)fadeTicks;
            if (reverse) lerp = 1 - lerp;

            curColour = Color.Lerp(initColour, finalColour, lerp);
        }

        public void setLoop(int loopQ)
        {
            loop = loopQ;
        }
    }

    public struct Line
    {
        public Vector2 p1;
        public Vector2 p2;

        public Line(Vector2 p1, Vector2 p2)
        {
            this.p1 = p1;
            this.p2 = p2;
        }

        public void AddX(float x)
        {
            p1.X += x;
            p2.X += x;
        }

        public void AddY(float y)
        {
            p1.Y += y;
            p2.Y += y;
        }

        public void Add(Vector2 v)
        {
            p1 += v;
            p2 += v;
        }

        public Vector2 Higher()
        {
            return p1.Y <= p2.Y ? p1 : p2;
        }

        public Vector2 Lower()
        {
            return p1.Y < p2.Y ? p2 : p1;
        }

        public Vector2 LeftMost()
        {
            return p1.X <= p2.X ? p1 : p2;
        }
        public Vector2 RightMost()
        {
            return p1.X < p2.X ? p2 : p1;
        }

        public float Length()
        {
            return Vector2.Distance(p1, p2);
        }

        public bool Intersects(Line l)
        {
            Vector2 CmP = l.p1 - p1;
            Vector2 r = p2 - p1;
            Vector2 s = l.p2 - l.p1;

            float CmPxr = CmP.X * r.Y - CmP.Y * r.X;
            float CmPxs = CmP.X * s.Y - CmP.Y * s.X;
            float rxs = r.X * s.Y - r.Y * s.X;

            if (CmPxr == 0f)
            {
                // Lines are collinear, and so intersect if they have any overlap

                return ((l.p1.X - p1.X < 0f) != (l.p1.X - p2.X < 0f))
                    || ((l.p1.Y - p1.Y < 0f) != (l.p1.Y - p2.Y < 0f));
            }

            if (rxs == 0f)
                return false; // Lines are parallel.

            float rxsr = 1f / rxs;
            float t = CmPxs * rxsr;
            float u = CmPxr * rxsr;

            if ((t >= 0f) && (t <= 1f) && (u >= 0f) && (u <= 1f)) {

                return true;
            } else
                return false;

        }

        public bool Intersects(Line l, out Vector2 pointOfIntersection)
        {
            Vector2 CmP = l.p1 - p1;
            Vector2 r = p2 - p1;
            Vector2 s = l.p2 - l.p1;
            pointOfIntersection = Vector2.Zero;
            float CmPxr = CmP.X * r.Y - CmP.Y * r.X;
            float CmPxs = CmP.X * s.Y - CmP.Y * s.X;
            float rxs = r.X * s.Y - r.Y * s.X;

            if (CmPxr == 0f)
            {
                // Lines are collinear, and so intersect if they have any overlap

                if (((l.p1.X - p1.X < 0f) != (l.p1.X - p2.X < 0f)))
                {
                    pointOfIntersection = l.p1;
                    return true;
                }
                else if (((l.p1.Y - p1.Y < 0f) != (l.p1.Y - p2.Y < 0f)))
                {
                    pointOfIntersection = l.p1;
                    return true;
                }
                else
                    return false;
            }

            if (rxs == 0f)
                return false; // Lines are parallel.

            float rxsr = 1f / rxs;
            float t = CmPxs * rxsr;
            float u = CmPxr * rxsr;

            if ((t >= 0f) && (t <= 1f) && (u >= 0f) && (u <= 1f))
            {
                pointOfIntersection = p1 + r * t;
                return true;
            }
            return false;

        }


        public bool Intersects(Rectangle r)
        {
            Vector2 topLeft = new Vector2(r.Left, r.Top);
            Vector2 topRight = new Vector2(r.Right, r.Top);
            Vector2 bottomLeft = new Vector2(r.Left, r.Bottom);
            Vector2 bottomRight = new Vector2(r.Right, r.Bottom);

            if (Intersects(new Line(topLeft, topRight)))
                return true;
            if (Intersects(new Line(topRight, bottomRight)))
                return true;
            if (Intersects(new Line(bottomRight, bottomLeft)))
                return true;
            //if (Intersects(new Line(bottomLeft, topLeft)))
            //    return true;
            return r.Contains(p1) || r.Contains(p2);
        }
        public bool Intersects(Rectangle r, out Vector2 pointOfIntersection)
        {
            Vector2 topLeft = new Vector2(r.Left, r.Top);
            Vector2 topRight = new Vector2(r.Right, r.Top);
            Vector2 bottomLeft = new Vector2(r.Left, r.Bottom);
            Vector2 bottomRight = new Vector2(r.Right, r.Bottom);

            if (Intersects(new Line(topLeft, topRight), out pointOfIntersection))
                return true;
            if (Intersects(new Line(topRight, bottomRight), out pointOfIntersection))
                return true;
            if (Intersects(new Line(bottomRight, bottomLeft), out pointOfIntersection))
                return true;
            if (Intersects(new Line(bottomLeft, topLeft), out pointOfIntersection))
                return true;
            return r.Contains(p1);
        }

        public bool Intersects(Polygon12 p, bool checkContains = false)
        {
            return p.Intersects(this, checkContains);
        }
        public bool Intersects(Polygon12 p, out Vector2 pointOfIntersection, bool checkContains = false)
        {
            return p.Intersects(this,out pointOfIntersection, checkContains);
        }


        public static Line FromRectLeft(Rectangle r)
        {
            return new Line(new Vector2(r.X, r.Y), new Vector2(r.X, r.Y + r.Height));
        }
        public static Line FromRectTop(Rectangle r)
        {
            return new Line(new Vector2(r.X, r.Y), new Vector2(r.X + r.Width, r.Y));
        }
        public static Line FromRectRight(Rectangle r)
        {
            return new Line(new Vector2(r.X + r.Width, r.Y), new Vector2(r.X + r.Width, r.Y + r.Height));
        }
        public static Line FromRectBottom(Rectangle r)
        {
            return new Line(new Vector2(r.X, r.Y+ r.Height), new Vector2(r.X + r.Width, r.Y + r.Height));
        }

    }



    public class Polygon12
    {
        /// <summary>
        /// The data in the poly12 class called 
        /// </summary>
        public Vector2[] point;


        public int numOfPoints=0;
        public const int maxNumOfPoints = 12;

        

        /// <summary>
        /// Default constructor (0,0), (0,0), (0,0), (0,0), ...maxNumOfPoints</summary>
        /// </summary>
        public Polygon12()
        {
            point = new Vector2[maxNumOfPoints];
            numOfPoints = maxNumOfPoints;
            //for (int i = 0; i < maxNumOfPoints; i++) // i beleive thgis happens automatically in C#
            //{
            //    point[i].X = 0;
            //    point[i].Y = 0;
            //}
        }

        public Polygon12(int noOfPoints)
        {
            point = new Vector2[maxNumOfPoints];
            numOfPoints = noOfPoints;
            //for (int i = 0; i < maxNumOfPoints; i++) // i beleive thgis happens automatically in C#
            //{
            //    point[i].X = 0;
            //    point[i].Y = 0;
            //}
        }

        /// <summary>
        /// Construct from Rectangle clockwise winding
        /// </summary>
        /// <param name="r"></param>
        public Polygon12(Rectangle r)
        {
            point = new Vector2[maxNumOfPoints];
            numOfPoints = 4;
            point[0].X = r.Left;
            point[0].Y = r.Top;

            point[1].X = r.Right;
            point[1].Y = r.Top;

            point[2].X = r.Right;
            point[2].Y = r.Bottom;

            point[3].X = r.Left;
            point[3].Y = r.Bottom;
        }

        /// <summary>
        /// Construct from Rect4 clockwise winding
        /// </summary>
        /// <param name="r"></param>
        public Polygon12(Rect4 r)
        {
            point = new Vector2[maxNumOfPoints];
            numOfPoints = 4;
            point[0].X = r.point[0].X;
            point[0].Y = r.point[0].Y;

            point[1].X = r.point[1].X;
            point[1].Y = r.point[1].Y;

            point[2].X = r.point[2].X;
            point[2].Y = r.point[2].Y;

            point[3].X = r.point[3].X;
            point[3].Y = r.point[3].Y;
        }

        public Polygon12(Vector2 center, int sides, float radius, float initialAngleRadians)
        {
            Vector2 p = new Vector2(center.X, center.Y + radius);
            Vector2 prev = new Vector2(0, 0);

            point = new Vector2[maxNumOfPoints];
            numOfPoints = sides;

            for (int i = 0; i < sides; i++)
            {
                Vector2 pp = Util.rotatePoint(p, center, (float)(Math.PI * 2 / (sides) * i)+initialAngleRadians);
                point[i].X = pp.X;
                point[i].Y = pp.Y;
                prev = pp;
            }
        }

        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="r"></param>
        public Polygon12(Polygon12 r) 
        {
            point = new Vector2[maxNumOfPoints];
            numOfPoints = r.numOfPoints;
            for (int i = 0; i < numOfPoints; i++)
            {
                point[i].X = r.point[i].X;
                point[i].Y = r.point[i].Y;
            }
        }

        /// <summary>
        /// Adds a point to the polygon silently fails if 12 points already used 
        /// </summary>
        /// <param name="pointQ"></param>
        public void addPoint(Vector2 pointQ)
        {
            if (numOfPoints >= maxNumOfPoints) return;
            point[numOfPoints].X = pointQ.X;
            point[numOfPoints].Y = pointQ.Y;
            numOfPoints++;
        }

        /// <summary>
        /// Adds a point to the polygon silently fails if 12 points already used 
        /// </summary>
         public void addPoint(float x, float y)
        {
            if (numOfPoints >= maxNumOfPoints) return;
            point[numOfPoints].X = x;
            point[numOfPoints].Y = y;
            numOfPoints++;
        }

        /// <summary>
        /// Rotates the Polygon12 by a given angle in radians
        /// </summary>
        /// <param name="centerOfRotation"></param>
        /// <param name="angleInRadians"></param>
        public void rotatePolygon12(Vector2 centerOfRotation, float angleInRadians)
        {
            for (int i = 0; i < numOfPoints; i++)
            {
                point[i] = Util.rotatePoint(point[i], centerOfRotation, -angleInRadians);
            }
        }

        /// <summary>
        /// Rotates the Polygon12 by a given angle in degrees
        /// </summary>
        /// <param name="centerOfRotation"></param>
        /// <param name="angleInDegrees"></param>
        public void rotatePolygon12Deg(Vector2 centerOfRotation, float angleInDegrees)
        {
            rotatePolygon12(centerOfRotation, angleInDegrees * (float)Math.PI / 180);
        }

        /// <summary>
        /// This returns an axis aligned bounding box based on the four corners of Rect4.
        /// The points should be a convex polygon, but this routine will work in all cases
        /// (note it can probably be done faster using the Max and Min functions but it deliberately this way so students can understand it)
        /// </summary>
        public Rectangle getAABoundingRect()
        {
            float Top = point[0].Y;
            float Left = point[0].X;
            float Bottom = point[0].Y;
            float Right = point[0].X;

            for (int i = 1; i < numOfPoints; i++) // start at 1 cause we did 0 in initialise
            {

                if (point[i].X < Left) Left = point[i].X;
                if (point[i].Y < Top) Top = point[i].Y;
                if (point[i].X > Right) Right = point[i].X;
                if (point[i].Y > Bottom) Bottom = point[i].Y;

            }
            // now have bounds in Top, left bottomm and right - covert to rectangle

            return new Rectangle((int)Left, (int)Top, (int)(Right - Left), (int)(Bottom - Top));
        }

        public bool Contains(Vector2 p)
        {
            float minX = point[0].X;
            float minY = point[0].Y;
            float maxX = point[0].X;
            float maxY = point[0].Y;

            for (int i = 1; i < numOfPoints; i++)
            {
                minX = Math.Min(point[i].X, minX);
                minY = Math.Min(point[i].Y, minY);
                maxX = Math.Max(point[i].X, maxX);
                maxY = Math.Max(point[i].Y, maxY);
            }

            if (p.X < minX || p.X > maxX || p.Y < minY || p.Y > maxY)
                return false;

            Line l = new Line(new Vector2(minX, p.Y), new Vector2(p.X, p.Y));
            int noOfTimes;
            if (Intersects(l, out noOfTimes))
            {
                if ((noOfTimes & 1) == 1)
                    return true;
                else
                    return false;
            }
            return false;


        }


        public bool Intersects(Line line,out Vector2 pointOfIntersection, bool checkContains = false)
        {
            for (int i = 0; i < numOfPoints - 1; i++)
            {
                Line polyLine = new Line(point[i], point[i + 1]);
                if (polyLine.Intersects(line, out pointOfIntersection))
                    return true;
            }
            if (line.Intersects(new Line(point[numOfPoints - 1], point[0]),out pointOfIntersection))
                return true;
            else if (!checkContains)
                return false;

            //Case where line is contained within the poly (if one end is then it is assumed the other is or the line would be intersecting with a line of the poly)
            if (Contains(line.p1))
                return true;
            return false;
        }

        public bool Intersects(Line line, bool checkContains = false)
        {
            for (int i = 0; i < numOfPoints-1; i++)
            {
                Line polyLine = new Line(point[i], point[i + 1]);
                if (polyLine.Intersects(line))
                    return true;
            }
            if (line.Intersects(new Line(point[numOfPoints - 1], point[0])))
                return true;
            else if (!checkContains)
                return false;

            if (Contains(line.p1) && Contains(line.p2))
                return true;
            return false;
        }

        public bool Intersects(Line line, out int numberOfTimes)
        {
            numberOfTimes = 0;
            for (int i = 0; i < numOfPoints - 1; i++)
            {
                Line polyLine = new Line(point[i], point[i + 1]);
                if (polyLine.Intersects(line))
                    numberOfTimes++;
            }
            if (line.Intersects(new Line(point[numOfPoints - 1], point[0])))
                numberOfTimes++;

            if (numberOfTimes > 0)
                return true;
            else
                return false;
        }

        public bool Intersects(Rectangle r)
        {
            Vector2 topLeft = new Vector2(r.Left, r.Top);
            Vector2 topRight = new Vector2(r.Right, r.Top);
            Vector2 bottomLeft = new Vector2(r.Left, r.Bottom);
            Vector2 bottomRight = new Vector2(r.Right, r.Bottom);

            if (Intersects(new Line(topLeft, topRight)))
                return true;
            if (Intersects(new Line(topRight, bottomRight)))
                return true;
            if (Intersects(new Line(bottomRight, bottomLeft)))
                return true;
            if (Intersects(new Line(bottomLeft, topLeft)))
                return true;
           return false;

        }

        public bool Intersects(Polygon12 p)
        {
            for (int i = 0; i < p.numOfPoints-1; i++)
            {
                Line polyLine = new Line(p.point[i], p.point[i + 1]);
                if (Intersects(polyLine))
                    return true;
            }
            if (Intersects(new Line(p.point[numOfPoints - 1], p.point[0])))
                return true;
            else
                return false;
        }

    }


// *********************************************** MakeTex Code Shell  *************************************************************************
    /*
    /// <summary>
    /// MakeTex a class for making square textures of a given colour - its a code shell not a class
    /// </summary>
    public class MakeTex
    {
        public static Texture2D rectangleBorder(GraphicsDevice device, Rectangle r, Color c, Color borderC, int borderWidth)
        {
            Texture2D tex = new Texture2D(device, r.Width, r.Height, false, SurfaceFormat.Color);
            Color[] data = new Color[r.Width * r.Height];

            //Color cLight = Util.lighterOrDarker(c, 1.4f);
            //Color cDark = Util.lighterOrDarker(c, 0.6f);

            for (int x = 0; x < r.Width; x++)
            {
            }
            return tex;
        }

    }
    */
}
