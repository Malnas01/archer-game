﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

using RC_Framework;
using Sprite3Extension;
using System.Threading;

namespace ArcherGame
{
    class Level3 : Level
    {

        Rectangle fallTrigger;
        Sprite3 grassyPlatform;
        bool fall;
        Portal portal;
        Sprite3 movingPlatform;
        Sprite3 movingPlatform2;
        Sprite3 movingPlatform3;
        Sprite3 movingPlatform4;

        public override void LoadLevel()
        {
            //Sprite3 s;

            SetLoadingText("Loading Textures");
            #region Load Textures
            Texture2D stairs = Content.Load<Texture2D>("stairs");
            Texture2D floatingPlatform = Content.Load<Texture2D>("floatingPlatform");
            Texture2D longPlatform = Content.Load<Texture2D>("platform2");
            Texture2D pillar = Content.Load<Texture2D>("pillar");
            Texture2D grass = Content.Load<Texture2D>("grass");
            Texture2D wall = Content.Load<Texture2D>("wall");
            Texture2D texs = Content.Load<Texture2D>("floatingPlatform");
            Texture2D arch = Content.Load<Texture2D>("arch");
            #endregion

            SetLoadingText("Loading Backdrop");
            #region Backdrop
            skybox.Add(new SkyboxImage(Content.Load<Texture2D>("Sky"), 0, 0, 0, 0));
            skybox.Add(new SkyboxImage(Content.Load<Texture2D>("mountains"), -15, 300, 40, 0));
            background.Add(new StaticImage(Content.Load<Texture2D>("mountains"), -11000, 0, 22000, 10000,Color.Gray, SpriteEffects.FlipHorizontally));
            background.Add(new StaticImage(pillar, 5750, 1540, 440, 3000 - 1540, new Color(0.2f, 0.2f, 0.2f)));
            background.Add(new StaticImage(pillar, 9100, 1540, 440, 3000 - 1540, new Color(0.2f, 0.2f, 0.2f)));
            background.Add(new StaticImage(pillar, 10820, 750, 440, 3000 - 750, new Color(0.2f, 0.2f, 0.2f)));
            background.Add(new StaticImage(pillar, 13622, -1925, 440, 3000 + 1925, new Color(0.2f, 0.2f, 0.2f)));
            #endregion

            SetLoadingText("Generating Platforms");
            #region Platforms

            //collideables.Add(LevelHelper.Stairs(stairs, -200, 3000, 3));
            
            collideables.Add(movingPlatform = LevelHelper.FloatingPlatform(floatingPlatform, 400, 2604));
            movingPlatform.setDeltaSpeed(new Vector2(7, 0));

            collideables.Add(movingPlatform2 = LevelHelper.FloatingPlatform(floatingPlatform, -10000, 4204));
            movingPlatform2.setDeltaSpeed(new Vector2(7, 0));

            collideables.Add(movingPlatform3 = LevelHelper.FloatingPlatform(floatingPlatform, -7500, 3104));
            movingPlatform3.setDeltaSpeed(new Vector2(-7, 0));
            movingPlatform3.flip = SpriteEffects.FlipVertically;

            collideables.Add(movingPlatform4 = LevelHelper.FloatingPlatform(floatingPlatform, 3889, 4901));
            movingPlatform4.setDeltaSpeed(new Vector2(0, -7));
            

            //collideables.Add(LevelHelper.FloatingPlatform(floatingPlatform, 3600, 1800));            
            
            collideables.Add(LevelHelper.LongPlatorm(longPlatform,5200,1100,3));

            Sprite3 stepper;
            collideables.Add(stepper = LevelHelper.FloatingPlatform(floatingPlatform, 3100, 2087));
            stepper.displayAngle = Util.degToRad(90);
            stepper.flip = SpriteEffects.FlipHorizontally;

            collideables.Add(LevelHelper.Arch(arch, -985, 1400, 3f, 3f));
            collideables.Add(grassyPlatform = LevelHelper.LongPlatorm(longPlatform, -1129, 3435, 3));
            collideables.Add(LevelHelper.PillarStraight(pillar, 2780, 1687, 3));

            Sprite3 slantedPlatform;
            collideables.Add(slantedPlatform = LevelHelper.LongPlatorm(longPlatform, -6000, 2735, 3));
            slantedPlatform.displayAngle = Util.degToRad(45);

            collideables.Add(LevelHelper.FloatingPlatform(floatingPlatform,-2797, 3967));

            //collideables.Add(s = LevelHelper.LongPlatorm(Content.Load<Texture2D>("platform"), 10703, 1000, 3));
            //s.displayAngle = (float)-MathHelper.PiOver4;
            
            //collideables.Add(s = LevelHelper.Stairs(stairs, 20000, -4520, 3));
            //s.displayAngle = MathHelper.PiOver2;

            //collideables.Add(LevelHelper.LongPlatorm(longPlatform, 16600, -3000, 3));

            
            foreground.Add(new StaticImage(grass, 190, 2720, null, null, SpriteEffects.FlipHorizontally));
            #endregion
            //#region Falling Pillar
            //fallingPillar = new Sprite3(true, pillar, 14075, -2421);
            //fallingPillar.setHSoffset(new Vector2(86, 571));
            //fallingPillar.setHeight(3000);
            //fallingPillar.setWidth(400);
            //collideables.Add(fallingPillar);
            //fallingPillar.boundingPolyOffsets = new Polygon12(0);
            //fallingPillar.boundingPolyOffsets.addPoint(new Vector2(0, 570));
            //fallingPillar.boundingPolyOffsets.addPoint(new Vector2(0, 548));
            //fallingPillar.boundingPolyOffsets.addPoint(new Vector2(30, 526));
            //fallingPillar.boundingPolyOffsets.addPoint(new Vector2(30, 58));
            //fallingPillar.boundingPolyOffsets.addPoint(new Vector2(0, 40));
            //fallingPillar.boundingPolyOffsets.addPoint(new Vector2(0, 17));
            //fallingPillar.boundingPolyOffsets.addPoint(new Vector2(165, 17));
            //fallingPillar.boundingPolyOffsets.addPoint(new Vector2(165, 40));
            //fallingPillar.boundingPolyOffsets.addPoint(new Vector2(136, 59));
            //fallingPillar.boundingPolyOffsets.addPoint(new Vector2(136, 536));
            //fallingPillar.boundingPolyOffsets.addPoint(new Vector2(165, 545));
            //fallingPillar.boundingPolyOffsets.addPoint(new Vector2(165, 570));

            //fallTrigger = new Rectangle(13690, -2800, 500, 500);
            //#endregion

            SetLoadingText("Spawning Monsters");
            #region Spawn Monsters
            enemies.Add(new Bat(-9615, 2506, Content, player));
            enemies.Add(new Ghost(-3277, 4667, Content, player, 3));
            enemies.Add(new Bat(2138, 3040, Content, player));
            enemies.Add(new Ghost(2769, 4425, Content, player, 3));
            #endregion

            SetLoadingText("Spawning Loot");
            #region Pickups
            collectables.Add(new ArrowHead(-6555, 2579, Content));
            collectables.Add(new ArrowHead(-8255, 4036, Content));
            collectables.Add(new ArrowHead(-10125, 2190, Content));
            collectables.Add(new ArrowHead(4445, 2870, Content));
            collectables.Add(new ArrowHead(1069, 4473, Content));
            collectables.Add(new ArrowHead(5416, 1875, Content));
            #endregion

            //SetLoadingText("Finalizing");
            #region Level Bounds

            Sprite3 ground = LevelHelper.Ground(LineBatch._empty_texture, -18192, 5000, 36384, 5000, Color.Black);
            collideables.Add(ground); 

            LevelHelper.AddGrass(grassyPlatform, foreground, grass, 0.1f, 0, 1, 0.5f, true, -10);

            collideables.Add(LevelHelper.Wall(wall, -10000, 5000, 4));
            collideables.Add(LevelHelper.Wall(wall, 10000, 5000, 4, SpriteEffects.FlipHorizontally));
            #endregion
            portal = new Portal(7578, 758, LevelManager, Content);

            #region NIU
            /*
             Sprite3 platform;
            platform = new Sprite3(true, Content.Load<Texture2D>("platform2"), 400, 265);
            platform.setHSoffset(new Vector2(956, 122));
            platform.boundingPolyOffsets = new Polygon12(4);
            platform.boundingPolyOffsets.point[0] = new Vector2(46, 46);
            platform.boundingPolyOffsets.point[1] = new Vector2(1651, 31);
            platform.boundingPolyOffsets.point[2] = new Vector2(1491, 200);
            platform.boundingPolyOffsets.point[3] = new Vector2(223, 207);
            platform.displayAngle = Util.degToRad(45);
            collidables.Add(platform);

            SetLoadingText("Loading Wall");
            Sprite3 wall = new Sprite3(true, Content.Load<Texture2D>("ground"), 600, 100);
            wall.displayAngle = MathHelper.PiOver2+0.1f;
            wall.setBoundingPolyFromBB();
            //wall.setBBToTexture();
            collidables.Add(wall);
            Thread.Sleep(150);

            collidables.Add(movingPlatform = new Sprite3(true, Content.Load<Texture2D>("ground"), 700, 100));
            movingPlatform.setDeltaSpeed(new Vector2(3,0));

            SetLoadingText("Loading Slope");
            slope  = new Sprite3(true, Content.Load<Texture2D>("ground"), 300, 100);
            slope.setHSoffset(new Vector2(128, 32));
            slope.displayAngle = -MathHelper.PiOver4*3;
            slope.setBBToTexture();
            slope.setBoundingPolyFromBB();
            collidables.Add(slope);
            Thread.Sleep(200);

            skybox.Add(new StaticImage(Content.Load<Texture2D>("Sky"),0, 0, (float)Game1.window.ClientBounds.Width, (float)Game1.window.ClientBounds.Height));
            
            enemies.Add(new Bat(500,100,Content,player));
            */
            #endregion
        }

        public override void SetUpLevel()
        {
            player.Reset(new Vector2(-5289, 4600));
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            movingPlatform.moveByDeltaXY();
            if (movingPlatform.getPosX() >= 800)
            {
                movingPlatform.setDeltaSpeed(new Vector2(-7, 0));
            }
            else if (movingPlatform.getPosX() <= -700)
            {
                movingPlatform.setDeltaSpeed(new Vector2(7, 0)); 
            }

            movingPlatform2.moveByDeltaXY();
            if (movingPlatform2.getPosX() >= -8000)
            {
                movingPlatform2.setDeltaSpeed(new Vector2(-7, 0));
            }
            else if (movingPlatform2.getPosX() <= -10000)
            {
                movingPlatform2.setDeltaSpeed(new Vector2(7, 0));
            }

            movingPlatform3.moveByDeltaXY();
            if (movingPlatform3.getPosX() >= -7500)
            {
                movingPlatform3.setDeltaSpeed(new Vector2(-7, 0));
            }
            else if (movingPlatform3.getPosX() <= -10000)
            {
                movingPlatform3.setDeltaSpeed(new Vector2(7, 0));
            }

            movingPlatform4.moveByDeltaXY();
            if (movingPlatform4.getPosY() >= 4301)
            {
                movingPlatform4.setDeltaSpeed(new Vector2(0, -7));
            }
            else if (movingPlatform4.getPosY() <= 3839)
            {
                movingPlatform4.setDeltaSpeed(new Vector2(0, 7));
            }

            

            //if (fall)
            //{
            //    if (fallingPillar.displayAngle < 1.287f)
            //    {
            //        fallingPillar.displayAngle += 0.001f + 0.02f * fallingPillar.displayAngle;
            //    }
            //    else
            //    {
            //        fallingPillar.displayAngle = 1.287f;
            //    }
            //} else if(fallTrigger.Contains(player.getPos()))
            //        fall = true;

            if (ArrowHead.CountIn(collectables) == 0)
                collectables.Add(portal);
        }

        public override void Draw(SpriteBatch sb)
        {
            base.Draw(sb);
            if (!loading)
            {
            }
        }

        public override void DrawGUI(SpriteBatch sb, int width, int height)
        {
            base.DrawGUI(sb, width, height);
        }

    }
}
