﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using RC_Framework;
using Sprite3Extension;

namespace ArcherGame
{
    static class LevelHelper
    {

        public static Sprite3 LongPlatorm(Texture2D tex, float x, float y, float scale = 1)
        {
            Sprite3 platform = new Sprite3(true, tex, x, y);
            
            platform.setWidth(platform.getWidth() * scale);
            platform.setHeight(platform.getHeight() * scale);
            platform.boundingPolyOffsets = new Polygon12(4);
            platform.boundingPolyOffsets.point[0] = new Vector2(46, 46);
            platform.boundingPolyOffsets.point[1] = new Vector2(1651, 31);
            platform.boundingPolyOffsets.point[2] = new Vector2(1491, 200);
            platform.boundingPolyOffsets.point[3] = new Vector2(223, 207);
            platform.setHSoffset(new Vector2(46,46));

            return platform;
        }

        public static Sprite3 Arch(Texture2D tex, float x, float y, float scaleX = 1, float scaleY = 1)
        {
            Sprite3 arch = new Sprite3(true, tex, x, y);

            arch.setWidth(arch.getWidth() * scaleX);
            arch.setHeight(arch.getHeight() * scaleY);
            arch.boundingPolyOffsets = new Polygon12(0);
            arch.boundingPolyOffsets.addPoint(new Vector2(1405, 716));
            arch.boundingPolyOffsets.addPoint(new Vector2(1410, 301));
            arch.boundingPolyOffsets.addPoint(new Vector2(1518, 388));
            arch.boundingPolyOffsets.addPoint(new Vector2(1825, 152));
            arch.boundingPolyOffsets.addPoint(new Vector2(1951, 164));
            arch.boundingPolyOffsets.addPoint(new Vector2(2092, 298));
            arch.boundingPolyOffsets.addPoint(new Vector2(2359, 373));
            arch.boundingPolyOffsets.addPoint(new Vector2(2263, 712));
            arch.boundingPolyOffsets.addPoint(new Vector2(2140, 712));
            arch.boundingPolyOffsets.addPoint(new Vector2(2075, 356));
            arch.boundingPolyOffsets.addPoint(new Vector2(1650, 356));
            arch.boundingPolyOffsets.addPoint(new Vector2(1588, 722));
            arch.setHSoffset(new Vector2(46, 46));

            return arch;
        }

        public static Sprite3 Pillar(Texture2D tex, float x, float y, float scale = 1)
        {
            Sprite3 platform = new Sprite3(true, tex, x, y);
            platform.setWidth(platform.getWidth() * scale);
            platform.setHeight(platform.getHeight() * scale);
            platform.boundingPolyOffsets = new Polygon12(0);
            platform.boundingPolyOffsets.addPoint(new Vector2(0, 570));
            platform.boundingPolyOffsets.addPoint(new Vector2(0, 548));
            platform.boundingPolyOffsets.addPoint(new Vector2(30, 526));
            platform.boundingPolyOffsets.addPoint(new Vector2(30, 58));
            platform.boundingPolyOffsets.addPoint(new Vector2(0, 40));
            platform.boundingPolyOffsets.addPoint(new Vector2(0, 17));
            platform.boundingPolyOffsets.addPoint(new Vector2(165, 17));
            platform.boundingPolyOffsets.addPoint(new Vector2(165, 40));
            platform.boundingPolyOffsets.addPoint(new Vector2(136, 59));
            platform.boundingPolyOffsets.addPoint(new Vector2(136, 536));
            platform.boundingPolyOffsets.addPoint(new Vector2(165, 545));
            platform.boundingPolyOffsets.addPoint(new Vector2(165, 570));
            platform.setHSoffset(new Vector2(86, 571));
            return platform;
        }

        public static Sprite3 PillarStraight(Texture2D tex, float x, float y, float scale = 1)
        {
            Sprite3 platform = new Sprite3(true,tex, x, y);
            platform.setWidth(platform.getWidth() * scale);
            platform.setHeight(platform.getHeight() * scale);
            platform.setBB(34, 16, 100, 555);
            return platform;
        }

        public static Sprite3 Wall(Texture2D tex, float x, float y, float scale = 1, SpriteEffects flip = SpriteEffects.None)
        {
            int wallIndentOffset = 24;
            Sprite3 platform = new Sprite3(true, tex, x, y);
            platform.setWidth(platform.getWidth() * scale);
            platform.setHeight(platform.getHeight() * scale);


            platform.flip = flip;
            if (flip == SpriteEffects.FlipHorizontally)
            {
                platform.setBB(wallIndentOffset, 0, tex.Width - wallIndentOffset, tex.Height);
                platform.setHSoffset(new Vector2(0, tex.Height));
            }
            else
            {
                platform.setBB(0, 0, tex.Width - wallIndentOffset, tex.Height);
                platform.setHSoffset(new Vector2(tex.Width, tex.Height));
            }



            return platform;
        }

        /// <summary>
        /// Requires 1200 gap for player to pass under
        /// </summary>
        /// <param name="tex"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="scale"></param>
        /// <returns></returns>
        public static Sprite3 FloatingPlatform(Texture2D tex, float x, float y, float scale = 1)
        {
            Sprite3 platform = new Sprite3(true, tex, x, y);
            platform.setWidth(platform.getWidth() * scale);
            platform.setHeight(platform.getHeight() * scale);
            platform.boundingPolyOffsets = new Polygon12(5);
            platform.boundingPolyOffsets.point[0] = new Vector2(36, 240);
            platform.boundingPolyOffsets.point[1] = new Vector2(295, 50);
            platform.boundingPolyOffsets.point[2] = new Vector2(1208, 50);
            platform.boundingPolyOffsets.point[3] = new Vector2(1421, 238);
            platform.boundingPolyOffsets.point[4] = new Vector2(557, 580);
            return platform;
        }

        public static Sprite3 Ground(Texture2D tex, float x, float y, float width, float height, Color color)
        {
            Sprite3 platform = new Sprite3(true, tex, x, y);
            platform.setWidthHeight(width, height);
            platform.setBBToTexture();
            platform.setColor(color);
            return platform;
        }

        public static void AddGrass(Sprite3 ground, List<StaticImage> layer,Texture2D tex, float density, int maxRandomOffset = 0, float xScale = 1, float yScale = 1,bool flipRandomly = true , float yOffset = 30 )
        {
            Random rng = new Random();

            int iterations = (int)(ground.getWidth() / tex.Width * xScale * density);
            float spacing = ground.getWidth() / iterations / density;

            for (int i = 0; i < iterations; i++)
            {
                layer.Add(new StaticImage(tex, MathHelper.Clamp(i * spacing + rng.Next(-maxRandomOffset, maxRandomOffset), ground.getPosX(), ground.getPosX() + ground.getWidth() - tex.Width * xScale), ground.getPosY() - tex.Height * yScale + yOffset, tex.Width * xScale, tex.Height * yScale, (rng.Next(2) == 1 ? SpriteEffects.FlipHorizontally : SpriteEffects.None)));
            }
        }


        public static Sprite3 Stairs(Texture2D tex, float x, float y, float scale)
        {
            Sprite3 platform = new Sprite3(true, tex, x, y);
            platform.setWidth(platform.getWidth() * scale);
            platform.setHeight(platform.getHeight() * scale);
            platform.boundingPolyOffsets = new Polygon12(10);
            platform.boundingPolyOffsets.point[0] = new Vector2(18, 15);
            platform.boundingPolyOffsets.point[1] = new Vector2(244, 16);
            platform.boundingPolyOffsets.point[2] = new Vector2(314, 71);
            platform.boundingPolyOffsets.point[3] = new Vector2(422, 77);
            platform.boundingPolyOffsets.point[4] = new Vector2(426, 121);
            platform.boundingPolyOffsets.point[5] = new Vector2(493, 138);
            platform.boundingPolyOffsets.point[6] = new Vector2(510, 191);
            platform.boundingPolyOffsets.point[7] = new Vector2(624, 210);
            platform.boundingPolyOffsets.point[8] = new Vector2(650, 255);
            platform.boundingPolyOffsets.point[9] = new Vector2(15, 255);
            platform.setHSoffset(new Vector2(15, 255));
            return platform;
        }

        public static void DrawGround(SpriteBatch sb, int screenWidth, int screenHeight, int groundHeight = 3000)
        {
            Vector2 pos = Vector2.Transform(new Vector2(0, groundHeight), PlayState.Camera.Transform);
            Rectangle rect = new Rectangle(0, (int)pos.Y, screenWidth, screenHeight);
            sb.Draw(LineBatch._empty_texture, rect, Color.Black);
        }
    }
}
