﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArcherGame
{
    class LevelManager
    {
        List<Level> levels;
        public int CurrLevelIndex { get; private set; }
        SpriteBatch sb;
        ContentManager c;
        Archer player;

        public LevelManager(SpriteBatch sb, ContentManager c, Archer player)
        {
            levels = new List<Level>();
            CurrLevelIndex = -1;
            this.sb = sb;
            this.c = c;
            this.player = player;
        }

        public Level CurrentLevel()
        {
            if (CurrLevelIndex < 0 || CurrLevelIndex >= levels.Count)
                return null;
            return levels[CurrLevelIndex];
        }

        public void SetLevel(int i)
        {
            if (CurrLevelIndex >= 0)
                CurrentLevel().UnloadLevel();
            if (levels.Count > 0 && i >= 0 && i < levels.Count)
            {
                CurrLevelIndex = i;
                CurrentLevel().CreateLevel(player, c);
            }
        }

        public void NextLevel()
        {
            if (CurrLevelIndex >= 0)
                CurrentLevel().UnloadLevel();
            if (CurrLevelIndex < levels.Count - 1)
                CurrLevelIndex++;

            CurrentLevel().CreateLevel(player, c);
        }

        public void AddLevel(Level level)
        {
            level.LevelManager = this;
            levels.Add(level);
        }

        public void AddLevel(Type levelClass)
        {
            Level l = (Level)Activator.CreateInstance(levelClass);
            l.LevelManager = this;
            levels.Add(l);
        }

        public int LevelCount()
        {
            return levels.Count;
        }
        public void RemoveLevel(Level level)
        {
            if (levels.IndexOf(level) == CurrLevelIndex)
            {
                levels.Remove(level);
                CurrLevelIndex--;
            }
            else
                levels.Remove(level);
        }
    }
}
