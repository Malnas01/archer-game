﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using RC_Framework;
using Sprite3Extension;

namespace ArcherGame
{
    class Portal : Collectable
    {

        LevelManager lm;
        static Texture2D texture;

        public Portal(float x, float y, LevelManager lm, ContentManager Content) : base(x,y)
        {
            this.lm = lm;
            if (texture == null || texture.IsDisposed)
                setTexture(texture = Content.Load<Texture2D>("portal"),true);
            else
                setTexture(texture, true);

            displayAngleDelta = 0.3f;

            setBBandHSFractionOfTexCentered(0.1f);

            OnCollect = delegate(Archer player)
            {
                lm.NextLevel();
            };

        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            adjustAngles();
        }
    }
}
