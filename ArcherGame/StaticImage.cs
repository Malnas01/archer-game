﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

using RC_Framework;
using Sprite3Extension;

namespace ArcherGame
{
    class StaticImage : RC_Renderable
    {

        public float xPos;
        public float yPos;
        public float width;
        public float height;
        public Color c;
        public Texture2D tex;
        public SpriteEffects flip;

        public StaticImage(Texture2D tex, float xPos, float yPos, float? width, float? height,SpriteEffects flip = SpriteEffects.None)
        {
            this.xPos = xPos;
            this.yPos = yPos;

            float scale = 0;
            if (width == null || height == null)
            {
                if (width != null)
                    scale = (float)width / tex.Width;
                if (height != null)
                    scale = (float)height / tex.Height;

                if (scale == 0)
                {
                    this.width = tex.Width;
                    this.height = tex.Height;
                }
                else
                {
                    this.width = tex.Width * scale;
                    this.height = tex.Height * scale;
                }


            }
            else
            {
                this.width = (float)width;
                this.height = (float)height;
            }


            this.tex = tex;
            this.flip = flip;
            this.c = Color.White;
        }

        public StaticImage(Texture2D tex, float xPos, float yPos, float? width, float? height, Color c, SpriteEffects flip = SpriteEffects.None)
            : this(tex, xPos, yPos, width, height, flip)
        {
            this.c = c;
        }

        public override void Draw(SpriteBatch sb)
        {
            sb.Draw(tex, destinationRectangle: new Rectangle((int)xPos, (int)yPos, (int)width, (int)height),color: c,effects: flip);
        }
    }
}
