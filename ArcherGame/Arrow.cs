﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

using RC_Framework;
using Sprite3Extension;

namespace ArcherGame
{
    class Arrow : Projectile
    {
        public static Texture2D texture;
        public static float gravity = 1.0f;
        public float dmg = 10;
        public float power;

        public Sprite3 attatched;
        public Vector2 attatchedOffset;
        public float attatchedRotationOffset;
        public float alpha = 0.8f;

        protected Line collisionLine;
        protected Line rotatedCollisionLine;
        bool dead;

        public Arrow(float x, float y, float speed,float angle)
            : base(true, texture, x, y)
        {
            deltaSpeed = new Vector2((float)Math.Cos(angle) * speed, (float)Math.Sin(angle) * speed);
            setHSoffset(new Vector2(176, 26));
            collisionLine = new Line(new Vector2(-65, 0),new Vector2(width - getHSOffset().X,0));
            rotatedCollisionLine = new Line(Util.rotatePoint(collisionLine.p1, Vector2.Zero, -displayAngle), Util.rotatePoint(collisionLine.p2, Vector2.Zero, -displayAngle));
            rotatedCollisionLine.Add(pos);
            setTicksToInvisible(1500, true);
            this.power = speed;
        }

        public override void Update(GameTime gameTime)
        {
            tick();
            if(TicksToInvisible > 0 && TicksToInvisible < 300)
                alpha = TicksToInvisible * 0.003f;
            if (!dead)
            {
                moveByDeltaXY();
                displayAngle = Util.getAngle(pos, oldPos);
                deltaSpeed.Y += gravity;
            }
            else if(attatched != null)
            {
                this.attatchTo(attatched, attatchedOffset,attatchedRotationOffset);
                if (attatched.active == false)
                {
                    this.active = false;
                    attatched = null;
                }
            }
        }

        public override void CollideEnemy(List<Enemy> enemies)
        {
            if (!dead)
            {
                foreach (Enemy e in enemies)
                {
                    if (e.Collides(rotatedCollisionLine))
                    {
                        e.Hurt(dmg,this);
                        StickInto(e);
                    }
                }
            }
        }

        public override void CollideLevel(List<Sprite3> collideables)
        {
            if (!dead) {

                Sprite3 other;

                if (IsColliding(collideables,out other))
                {
                    StickInto(other);
                }         
            }

        }

        void StickInto(Sprite3 s)
        {
            Stop();
            attatched = s;
            s.computeTexToWorldSpaceMatrix();
            attatchedOffset = Vector2.Transform(pos, s.WorldToTexSpaceMatrix);//Util.rotatePoint(pos, s.getPos(), s.displayAngle) - s.getPos();
            attatchedRotationOffset = displayAngle -s.displayAngle;
        }

        void Stop()
        {
            deltaSpeed = Vector2.Zero;
            dead = true;
        }

        private bool IsColliding(List<Sprite3> collideables, out Sprite3 other)
        {
            rotatedCollisionLine = new Line(Util.rotatePoint(collisionLine.p1, Vector2.Zero, -displayAngle), Util.rotatePoint(collisionLine.p2, Vector2.Zero, -displayAngle));
            rotatedCollisionLine.Add(pos);
            other = null;
            foreach (Sprite3 c in collideables)
            {
                other = c;
                if (c.boundingPoly != null)
                {
                    if (rotatedCollisionLine.Intersects(c.boundingPoly))
                        return true;
                }
                else if (rotatedCollisionLine.Intersects(c.getBoundingBoxAA()))
                    return true;
            }
            return false;
        }

        public override void Draw(SpriteBatch sb)
        {
            setColor(Color.White * alpha);
            base.Draw(sb);
        }

        public override void drawBB(SpriteBatch sb, Color c)
        {
            LineBatch.drawLine(sb, c, rotatedCollisionLine.p1, rotatedCollisionLine.p2);
        } 


    }
}
