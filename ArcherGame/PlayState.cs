﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

using RC_Framework;
using Sprite3Extension;
using System.Threading;
using Microsoft.Xna.Framework.Media;
namespace ArcherGame
{
    class PlayState : RC_GameStateParent
    {

        Archer player;
        LevelManager levelManager;

        public static ChaseCam Camera;
        SpriteFont font1;

        Song bgMusic;

        Texture2D cursor;


        Texture2D hpBox;
        bool pause = false;
        

        public override void LoadContent()
        {
            player = new Archer(Content, new Vector2(400, 200));
            hpBox = Content.Load<Texture2D>("healthbar");


            Camera = new ChaseCam(Window, player);
            Camera.Zoom = 0.2f;
            cursor = Content.Load<Texture2D>("arrow");

            font1 = Content.Load<SpriteFont>("SpriteFont1");
            levelManager = new LevelManager(spriteBatch, Content, player);
            levelManager.AddLevel(typeof(Level1));
            levelManager.AddLevel(typeof(Level3));
            levelManager.AddLevel(typeof(Level2));
            levelManager.AddLevel(new Win(delegate() { gameStateManager.setLevel(0); }));
            bgMusic = Content.Load<Song>("The Dark Amulet");
        }

        public override void UnloadContent()
        {
        
        }

        public override void EnterLevel(int fromLevelNum)
        {
            player.NewGame();
            if (!pause)
            {
                base.EnterLevel(fromLevelNum);
                levelManager.SetLevel(0);
                MediaPlayer.Play(bgMusic);
                MediaPlayer.Volume = 0.98f;
            }
            else pause = false;
            
        }

        public override void Update(GameTime gameTime) 
        {

            getKeyboardAndMouse();
            if (keyState.IsKeyDown(Keys.F1) && !prevKeyState.IsKeyDown(Keys.F1))
            {
                pause = true;
                gameStateManager.setLevel(2);
            }

            levelManager.CurrentLevel().DoUpdate(gameTime);

            Camera.Update();
        }
        public override void Draw(GameTime gameTime)
        {
            graphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();
            levelManager.CurrentLevel().DrawSkybox(spriteBatch, Window.ClientBounds.Width, Window.ClientBounds.Height);
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, Camera.Transform);
            levelManager.CurrentLevel().Draw(spriteBatch);
            spriteBatch.End();

            spriteBatch.Begin();

            spriteBatch.Draw(LineBatch._empty_texture, new Rectangle(10, 10, (int)(256 * player.health / player.maxHealth), 32), Color.Red);
            spriteBatch.Draw(hpBox, new Rectangle(10, 10, 256, 32), Color.White);
            levelManager.CurrentLevel().DrawGUI(spriteBatch, Window.ClientBounds.Width, Window.ClientBounds.Height);

            spriteBatch.End();
        }
    }
}
